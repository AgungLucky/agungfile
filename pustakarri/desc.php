<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title>Admin</title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">

			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">
			
			<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
			<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
			<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>

		<body>
		<!-- Header -->
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#topWrap">
							<span class="fa-stack fa-lg">
								
							</span>
							<span>
							<img src="themes/image/we.png" ><font size="+2" font color="#0066FF"> Pustaka Siar RRI Bandung </font>
							</span>
						</a>
					</div>
					<div class="collapse navbar-collapse appiNav">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Beranda</a></li>
							<li><a href="#productWrap">Lagu</a></li>
							<li><a href="#pricingWrap">Artis</a></li>
							<li><a href="#testimonialsWrap">Album</a></li>
							<li><a href="#contactWrap">Logout</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
				<br>
			</div>
			<!-- // Header -->
<br>
<br>
<br>
<br>
<br>

<?php
	$id_perpus_lagu = $_GET['id'];
	include('koneksi/koneksi.php');
	$link = koneksi_db(); 
	
	$sql_nama_album = "Select nama_album from t_album";
	$result_nama_album = mysqli_query($link, $sql_nama_album);

	$sql_tahun_album = "Select tahun_album from t_album";
	$result_tahun_album = mysqli_query($link, $sql_tahun_album);

	$sql_penyanyi = "Select penyanyi from t_album";
	$result_penyanyi = mysqli_query($link, $sql_penyanyi);

	$sql_file_src = "Select file_src from t_album";
	$result_fungsi_musik = mysqli_query($link, $sql_fungsi_musik);

	$sql_judul_lagu = "Select judul_lagu from t_lagu";
	$result_judul_lagu = mysqli_query($link, $sql_judul_lagu);

		$sql_ambil_data = '
	SELECT DISTINCT
t_perpus_lagu.kode_konten_umum, t_perpus_lagu.kode_sumber_data, t_perpus_lagu.kode_jenis_musik, m_detail_fungsi_musik.kode_fungsi_musik, m_detail_fungsi_musik.kode, 
m_detail_fungsi_musik.id as id_detail_musik,
IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
			(SELECT m_jenis_vocal.kode FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
			(SELECT m_jenis_instrument.kode FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)) as kode_vocal_intrument,
			IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
			(SELECT m_jenis_vocal.text FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
			(SELECT m_jenis_instrument.text FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)) as text_vocal_intrument,
			t_perpus_lagu.no_urut,
			t_perpus_lagu.id_jenis_penyimpanan,
			t_album.nama_album, t_perpus_lagu.id, t_album.tahun_album, t_album.deskripsi, t_album.id as id_album, 
	t_perpus_lagu.id as id_perpus_lagu,

	CONCAT(
	t_album.nama_album, ".",
	t_album.tahun_album, ".",
	t_album.penyanyi, ".",
	t_lagu.file_src,".",
	t_lagu.judul_lagu,".",
			
		FROM
			t_perpus_lagu
			INNER JOIN m_detail_fungsi_musik ON m_detail_fungsi_musik.id = t_perpus_lagu.id_detail_fungsi_musik
			INNER JOIN t_album ON t_album.id = t_perpus_lagu.id_album
			LEFT JOIN t_lagu ON t_lagu.id_album = t_album.id

	WHERE
		t_album.id = '.$id.'
 '; 

 $result_ambil_data = mysqli_query($link,$sql_ambil_data);

 $data_row = mysqli_fetch_array($result_ambil_data);



?>

<div class="row" style="margin-top:50px">
  <div class="col-md-3"></div>
  <div class="col-md-6">
  		<h2 align="center">Input Album</h2>
  		<form method=post class="form-horizontal" enctype="multipart/form-data" >
  			<input type="hidden" name="id_album" value="<?php echo $data_row['id_album'];?>">
  			<input type="hidden" name="id_perpus_lagu" value="<?php echo $data_row['id_perpus_lagu'];?>">
	  		<!-- Pilih konten umum -->
	  		<div class="form-group">
				<label for="konten_umum" class="col-sm-4 control-label">Konten Umum</label>
				<div class="col-sm-8">
					<select id="konten_umum" name="konten_umum" class="form-control">
						<option value="">Pilih Konten Umum</option>
						<?php 
							while ($row = mysqli_fetch_array($result_konten_umum)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>

			<!-- Pilih sumber data -->
			<div class="form-group">
				<label for="Sumber_data" class="col-sm-4 control-label">Sumber Data</label>
				<div class="col-sm-8">
					<select onChange="ambil_form_asing()" id="sumber_data" name="sumber_data" class="form-control">
						<option value="">Pilih Sumber Data</option>
						<?php 
							while ($row = mysqli_fetch_array($result_sumber_data)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>
			<div id="tmp_form_asing"></div>

			<!-- Pilih Jenis kategori-->
			<div class="form-group">
				<label for="kategori_musik" class="col-sm-4 control-label">Jenis Kategori Musik</label>
				<div class="col-sm-8">
					<select onChange="ambil_form_vocal_instrumen()" id="kategori_musik" name="kategori_musik" class="form-control">
						<option value="">Jenis Kategori Musik</option>
						<?php 
							while ($row = mysqli_fetch_array($result_jenis_musik)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>
			<div id="tmp_form_vocal"></div>

			<!-- Pilih Fungsi Musik -->
			<div class="form-group">
				<label for="fungsi_musik" class="col-sm-4 control-label">Fungsi Jenis Musik</label>
				<div class="col-sm-8">
					<select onChange="ambil_fungsi_musik()" id="fungsi_musik" name="fungsi_musik" class="form-control">
						<option value="">Fungsi Jenis Musik</option>
						<?php 
							while ($row = mysqli_fetch_array($result_fungsi_musik)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>

			<!--  Detail Fungsi Musik-->
			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Detail Fungsi Jenis Musik</label>
				<div class="col-sm-8">
					<select id="detail_musik" class="form-control" name="detail_musik">
						<option value="">Detail Fungsi Jenis Musik</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Nomor Urut</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="nomor_urut" name="nomor_urut" value="<?php echo $data_row['no_urut'];?> ">
				</div>
			</div>

			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Album</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="album" name="album" value="<?php echo $data_row['nama_album'];?>">
				</div>
			</div>

			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Tahun Pembuatan</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="tahun" name="tahun" value="<?php echo $data_row['tahun_album'];?>">
				</div>
			</div> 

			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Deskripsi</label>
				<div class="col-sm-8">
					<textarea class="form-control" id="Deskripsi" name="Deskripsi"><?php echo $data_row['deskripsi'];?></textarea>
				</div>
			</div>

	<!--		<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Gambar Album</label>
				<div class="col-sm-8">
					<input type="file"  id="gambar" name="gambar">
				</div>
			</div>-->
				<button type="submit" class="btn btn-info pull-right">Ubah</button>
  		</form>
  		
  </div>
  <div class="col-md-3"></div>
</div>
 



	
			<?php include "nav/footer.php" ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.js"></script>
	
<script type="text/javascript">

$(document).ready(function() {

	$('select[name=konten_umum]').val('<?php echo $data_row['kode_konten_umum'] ?>');
	$('select[name=sumber_data]').val('<?php echo $data_row['kode_sumber_data'] ?>');
	$('select[name=kategori_musik]').val('<?php echo $data_row['kode_jenis_musik'] ?>');
	
	$('select[name=fungsi_musik]').val('<?php echo $data_row['kode_fungsi_musik'] ?>');
	
	ambil_fungsi_musik();

	

	//$('select[name=vocal_instrumen]').val('');
	//$('select[name=detail_musik]').val('');
	
	//$('.selectpicker').selectpicker('refresh')


	$('.mobileSlider').flexslider({
		animation: "slide",
		slideshowSpeed: 3000,
		controlNav: false,
		directionNav: true,
		prevText: "&#171;",
		nextText: "&#187;"
	});
	$('.flexslider').flexslider({
		animation: "slide",
		directionNav: false
	});
		
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if ($(window).width() < 768) {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar-header').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}
			else {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}

		}
	});
	
	$('#toTop').click(function() {
		$('html,body').animate({
			scrollTop: 0
		}, 1000);
	});
	
	var timer;
    $(window).bind('scroll',function () {
        clearTimeout(timer);
        timer = setTimeout( refresh , 50 );
    });
    var refresh = function () {
		if ($(window).scrollTop()>100) {
			$(".tagline").fadeTo( "slow", 0 );
		}
		else {
			$(".tagline").fadeTo( "slow", 1 );
		}
    };

    
    ambil_form_vocal_instrumen('<?php echo $data_row['kode_vocal_intrument'] ?>','<?php echo $data_row['id_detail_musik'] ?>');
   
});
 function ambil_fungsi_musik()
  {
      $("#detail_musik").empty();
      var value = $("#fungsi_musik").val();
      $.post('ambil_fungsi_musik.php', {
            id: value,
            //kode:kode
      }, function(data, status){
          //alert(data);
          $("#detail_musik").html(data);

      });
  }

  
  function ambil_form_vocal_instrumen(kode, kode_detail)
  {
      $("#tmp_form_vocal").empty();
      var value = $("#kategori_musik").val();
      $.post('ambil_form_vocal_instrumen.php', {
            id: value,
            kode: kode,
            kode_detail: kode_detail
      }, function(data, status){
          //alert(data);
          $("#tmp_form_vocal").html(data);

      });
  }

  function ambil_form_asing()
  {
  	$("#tmp_form_asing").empty();
      var value = $("#sumber_data").val();
      $.post('ambil_form_asing.php', {
            id: value
      }, function(data, status){
          //alert(data);
          $("#tmp_form_asing").html(data);

      });
  }


</script>
  </body>
</html>