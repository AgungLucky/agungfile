<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title>Tambah Data</title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">

			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">
			
			<link href="assets/fa/css/font-awesome.css" rel="stylesheet">
			<link href='assets/font-google/font1.css' rel='stylesheet' type='text/css'>
			<link href='assets/font-google2/font2.css' rel='stylesheet' type='text/css'>

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>

		<body>
		<!-- Header -->
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#topWrap">
							<span class="fa-stack fa-lg">
								
							</span>
							<span>
							<img src="themes/image/we.png" ><font size="+2" font color="#0066FF"> Pustaka Siaran RRI Bandung </font>
							</span>
						</a>
					</div>
					<div class="collapse navbar-collapse appiNav">
						<ul class="nav navbar-nav">
							<li><a href="halaman_utama.php">Beranda</a></li>
							<li><a href="index.php">Logout</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
				<br>
			</div>
			<!-- // Header -->
<br>
<br>
<br>
<br>
<br>

<?php
	include('koneksi/koneksi.php');
	$link = koneksi_db(); 
	
	$sql_konten_umum = "Select * from m_konten_umum";
	$result_konten_umum = mysqli_query($link, $sql_konten_umum);

	$sql_sumber_data = "Select * from m_sumber_data";
	$result_sumber_data = mysqli_query($link, $sql_sumber_data);

	$sql_jenis_musik = "Select * from m_jenis_musik";
	$result_jenis_musik = mysqli_query($link, $sql_jenis_musik);

	$sql_fungsi_musik = "Select * from m_fungsi_musik";
	$result_fungsi_musik = mysqli_query($link, $sql_fungsi_musik);

	$sql_detail_fungsi_musik = "Select * from m_detail_fungsi_musik";
	$result_detail_fungsi_musik = mysqli_query($link, $sql_detail_fungsi_musik);

	$sql_jenis_penyimpanan = "Select * from m_jenis_penyimpanan";
	$result_detail_penyimpanan = mysqli_query($link, $sql_jenis_penyimpanan);

	$sql_id = "select id from t_perpus_lagu";
	$res = mysqli_query($link, $sql_id);


?>

<div class="row" style="margin-top:50px">
  <div class="col-md-3"></div>
  <div class="col-md-6">
  		<h2 align="center">Input Pengkodean Musik</h2>
  		<br>
  		<form method=post action="prosestambah.php" class="form-horizontal" enctype="multipart/form-data" >

  			<div class="form-group">
				
				<div class="col-sm-7">		
					<input type="hidden" class="form-control" id="id" name="id" value =""  >
				</div>
			</div>

			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Nama Album</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" id="album" name="album">
				</div>
			</div>

			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Tahun Pembuatan</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" id="tahun" name="tahun">
				</div>
			</div> 

			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Deskripsi</label>
				<div class="col-sm-7">
					<textarea class="form-control" id="Deskripsi" name="Deskripsi"></textarea>
				</div>
			</div>
			
	  		<!-- Pilih konten umum -->
	  		<div class="form-group">
				<label for="konten_umum" class="col-sm-4 control-label">Konten Umum</label>
				<div class="col-sm-7">
					<select id="konten_umum" name="konten_umum" class="form-control">
						<option value="">Pilih Konten Umum</option>
						<?php 
							while ($row = mysqli_fetch_array($result_konten_umum)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>

			<!-- Pilih sumber data -->
			<div class="form-group">
				<label for="Sumber_data" class="col-sm-4 control-label">Sumber Data</label>
				<div class="col-sm-7">
					<select onchange="ambil_form_asing()" id="sumber_data" name="sumber_data" class="form-control">
						<option value="">Pilih Sumber Data</option>
						<?php 
							while ($row = mysqli_fetch_array($result_sumber_data)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>
			<div id="tmp_form_asing"></div>

			<!-- Pilih Jenis kategori-->
			<div class="form-group">
				<label for="kategori_musik" class="col-sm-4 control-label">Jenis Kategori Musik</label>
				<div class="col-sm-7">
					<select onchange="ambil_form_vocal_instrumen()" id="kategori_musik" name="kategori_musik" class="form-control">
						<option value="">Jenis Kategori Musik</option>
						<?php 
							while ($row = mysqli_fetch_array($result_jenis_musik)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>
			<div id="tmp_form_vocal"></div>

			<!-- Pilih Fungsi Musik -->
			<div class="form-group">
				<label for="fungsi_musik" class="col-sm-4 control-label">Fungsi Jenis Musik</label>
				<div class="col-sm-7">
					<select onchange="ambil_fungsi_musik()" id="fungsi_musik" name="fungsi_musik" class="form-control">
						<option value="">Fungsi Jenis Musik</option>
						<?php 
							while ($row = mysqli_fetch_array($result_fungsi_musik)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>

			<!--  Detail Fungsi Musik-->
			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Detail Fungsi Jenis Musik</label>
				<div class="col-sm-7">
					<select id="detail_musik" class="form-control" name="detail_musik">
						<option value="">Detail Fungsi Jenis Musik</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="jenis_penyimpanan" class="col-sm-4 control-label">Jenis Penyimpanan</label>
				<div class="col-sm-7">
					<select id="jenis_penyimpanan" name="jenis_penyimpanan" class="form-control">
						<option value="">Jenis Penyimpanan</option>
						<?php 
							while ($row = mysqli_fetch_array($result_detail_penyimpanan)) {
								echo "<option value='".$row['id']."'>".$row['jenis_penyimpanan']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>


			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Gambar Album</label>
				<div class="col-sm-7">
					<input type="file"  id="gambar" name="gambar">
				</div>
			</div>
				<button type="submit" class="btn btn-info pull-right">Lanjutkan</button>
				<br>
				<br>
  		</form>
  		
  </div>
  <div class="col-md-3"></div>
</div>
 



	
			<?php include "nav/footer.php" ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.js"></script>
	
<script type="text/javascript">
$(document).ready(function() {

	$('.mobileSlider').flexslider({
		animation: "slide",
		slideshowSpeed: 3000,
		controlNav: false,
		directionNav: true,
		prevText: "&#171;",
		nextText: "&#187;"
	});
	$('.flexslider').flexslider({
		animation: "slide",
		directionNav: false
	});
		
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if ($(window).width() < 768) {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar-header').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}
			else {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}

		}
	});
	
	$('#toTop').click(function() {
		$('html,body').animate({
			scrollTop: 0
		}, 1000);
	});
	
	var timer;
    $(window).bind('scroll',function () {
        clearTimeout(timer);
        timer = setTimeout( refresh , 50 );
    });
    var refresh = function () {
		if ($(window).scrollTop()>100) {
			$(".tagline").fadeTo( "slow", 0 );
		}
		else {
			$(".tagline").fadeTo( "slow", 1 );
		}
    };

    

   
});
 function ambil_fungsi_musik()
  {
      $("#detail_musik").empty();
      var value = $("#fungsi_musik").val();
      $.post('ambil_fungsi_musik.php', {
            id: value
      }, function(data, status){
          //alert(data);
          $("#detail_musik").html(data);

      });
  }

  
  function ambil_form_vocal_instrumen()
  {
      $("#tmp_form_vocal").empty();
      var value = $("#kategori_musik").val();
      $.post('ambil_form_vocal_instrumen.php', {
            id: value
      }, function(data, status){
          //alert(data);
          $("#tmp_form_vocal").html(data);

      });
  }

  function ambil_form_asing()
  {
  	$("#tmp_form_asing").empty();
      var value = $("#sumber_data").val();
      $.post('ambil_form_asing.php', {
            id: value
      }, function(data, status){
          //alert(data);
          $("#tmp_form_asing").html(data);

      });
  }

</script>
  </body>
</html>