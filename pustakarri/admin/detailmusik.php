<!DOCTYPE html>
<html>
<head>
	<title>Detail Musik</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title>Tambah Musik Lanjutan</title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">
			<link href="css/data_table.css" rel="stylesheet">

			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">
			
			<link href="assets/fa/css/font-awesome.css" rel="stylesheet">
			<link href='assets/font-google/font1.css' rel='stylesheet' type='text/css'>
			<link href='assets/font-google2/font2.css' rel='stylesheet' type='text/css'>

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
</head>
<body>

<body>
		<!-- Header -->
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#topWrap">
							<span class="fa-stack fa-lg">
								
							</span>
							<span>
							<img src="themes/image/we.png" ><font size="+2" font color="#0066FF"> Pustaka Siaran RRI Bandung </font>
							</span>
						</a>
					</div>
					<div class="collapse navbar-collapse appiNav">
						<ul class="nav navbar-nav">
							<li><a href="halaman_utama.php">Beranda</a></li>
							<li><a href="tampildata.php">Tampil Data Musik</a></li>
							<li><a href="index.php">Logout</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
				<br>
			</div>
			<!-- // Header -->
<br>
<br>
<br>
<br>
<br>
<br>


<?php 
	include('koneksi/koneksi.php');
	$link = koneksi_db(); 
	$id = $_GET['id'];


			$sql_ambil_album = "Select nama_album, tahun_album, Deskripsi, id from t_album where id=$id";
			$select_album = mysqli_query($link, $sql_ambil_album);
			$hasil = mysqli_fetch_array($select_album);

			$sql_nama_album = "Select nama_album, tahun_album, Deskripsi, id from t_album where id=$id";
			$result_nama_album = mysqli_query($link, $sql_nama_album);

			$sql_tampil = "SELECT id, judul_lagu, penyanyi, file_src, durasi, id FROM t_lagu WHERE id_album = $id";

			$result = mysqli_query($link, $sql_tampil);

	 		$banyakrecord = mysqli_num_rows($result);
	 		if($banyakrecord>0){ ?>
<br>
<div class="row" style="margin-top:10px">
    	<div class="col-md-0"></div>
    	<div class="col-md-12">		



    	<!-- table-->
			<table border="0" style="margin-left:30px">
				<tr>
					<td style="font-size:17px" width="200px" align=""><b>Nama Album<b></td>
					<td style="font-size:17px" width="200px" align=""><?php  echo $hasil['nama_album'];?></td>
				</tr>
				<tr>
					<td style="font-size:17px" width="200px" align=""><b>Tahun Rilis<b></td>
					<td style="font-size:17px" width="200px" align=""><?php  echo $hasil['tahun_album'];?></td>
				</tr>
				<tr>
					<td style="font-size:17px" width="200px" align=""><b>Deskripsi<b></td>
					<td style="font-size:17px" width="900px" align=""><p align="justify"><?php  echo $hasil['Deskripsi'];?></p></td>
				</tr>
			</table>
			<br>
<!--Modals -->
    	<div class="bd-example">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Tambah Data Musik</button>	
			<br>
			<br>		
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			    <div class="modal-dialog" role="document">
			      <div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			            
			          </button>
			          <h4 class="modal-title" id="exampleModalLabel">Form Tambah Lagu</h4>
			        </div>
			        <div class="modal-body">
			        <!-- Isi Form -->
			          <form method=post action="prosestambah_detailmusik.php" enctype="multipart/form-data">

			            <input type="hidden" name="id_album" value="<?php echo $hasil['id'];?>">

			          <div class="form-group">
			              <label for="recipient-name" class="form-control-label">Nama Album</label>
			              <input type="text" class="form-control" id="nama_album" name="nama_album" value ="<?php echo $hasil['nama_album'];?>" readonly='readonly' >
			            </div> 

			            <div class="form-group">
			              <label for="recipient-name" class="form-control-label">Judul Lagu</label>
			              <input type="text" class="form-control" id="judul" name="judul">
			            </div>

			             <div class="form-group">
			              <label for="recipient-name" class="form-control-label">Penyanyi</label>
			              <input type="text" class="form-control" id="penyanyi" name="penyanyi">
			            </div>

			             <div class="form-group">
			              <label for="recipient-name" class="form-control-label">Durasi</label>
			              <input type="text" class="form-control" id="durasi" name="durasi">
			            </div>

			            <div class="form-group">
							<label for="detail_musik" class="form-control-label">Musik</label>
							<input type="file"  id="lagu" name="lagu">
						</div>

						<div class="modal-footer">
					          <button type="button" class="btn btn-secondary" data-dismiss="modal">Reset</button>
					          <button type="submit" class="btn btn-primary">Simpan</button>
				        </div>
			          </form>
			        </div>
			        <!-- Button Tambah -->
			        
			      </div>
			    </div>
			  </div>
			</div>
		<!-- /modals -->
			<br>
			<br>
				<div align="center">Data Kategori ditemukan sebanyak : <b><?php echo $banyakrecord;?> </b>Record</div> 
				
						<table id="example" style="font-size: medium; margin-left: 0px;">
							<thead style="background-color: #0066FF; color: white;">
								<tr>				
									<th align="center">Penyanyi</th>
									<th align="center">Judul</th>
									<th align="center">Musik</th>
									<th align="center">Durasi</th>
									<th></th>
									<th align="center"> Aksi </th>
									
								</tr> 
							</thead>
							<tbody>
								<?php
								$i=0; 
								while( $data=mysqli_fetch_array($result)){ 
								$i++;
								 ?>  
								 <tr>
									  <td align="center"><?php echo $data['penyanyi'];?></td> 
									  <td align="center"><?php echo $data['judul_lagu'];?></td>
									  <td align="center"><?php echo $data['file_src'];?></td>
									  <td align="center"><?php echo $data['durasi'];?></td>
									  <td>
									  		<audio controls>
											  <source src="<?php echo "mp3/".$data ['file_src']; ?>" type="audio/mpeg">
											</audio>
									  </td>
									  <td><a href ="proseshapus_detail_musik.php?id=<?php echo $data['id'];?>&link=<?php echo $id;?>"><button type="button" class="btn btn-primary">Hapus</button></a><a href ="ubahdetail_musik.php?id=<?php echo $data['id'];?>&link=<?php echo $id;?>"><button type="button" class="btn btn-primary">Ubah</button></a>
									  </td>
								  </tr>
								  <?php
								  
								  } 
								  ?>
								  </tbody>
								   </table> 
						   <?php
						   } else echo "Tidak ada data pada tabel Kategori."; 
						   ?>
						
						<div class="modal-footer">
					          <a href ="tampildata.php"><button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button></a>
				        </div>

</form>
</div>
    	<div class="col-md-0"></div>

			<script src="js/jquery.min.js"></script>
			<script src="js/bootstrap.min.js"></script>
			<script src="js/flexslider.js"></script>
			<script src="js/data_table.js"></script>

			<script type="text/javascript">

			    
			     $(document).ready(function() {
			    $('#example').DataTable();
			    });
			  
			$(document).ready(function() {

				$('.mobileSlider').flexslider({
					animation: "slide",
					slideshowSpeed: 3000,
					controlNav: false,
					directionNav: true,
					prevText: "&#171;",
					nextText: "&#187;"
				});
				$('.flexslider').flexslider({
					animation: "slide",
					directionNav: false
				});
					
				$('a[href*=#]:not([href=#])').click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						if ($(window).width() < 768) {
							if (target.length) {
								$('html,body').animate({
									scrollTop: target.offset().top - $('.navbar-header').outerHeight(true) + 1
								}, 1000);
								return false;
							}
						}
						else {
							if (target.length) {
								$('html,body').animate({
									scrollTop: target.offset().top - $('.navbar').outerHeight(true) + 1
								}, 1000);
								return false;
							}
						}

					}
				});
				
				$('#toTop').click(function() {
					$('html,body').animate({
						scrollTop: 0
					}, 1000);
				});
				
				var timer;
			    $(window).bind('scroll',function () {
			        clearTimeout(timer);
			        timer = setTimeout( refresh , 50 );
			    });
			    var refresh = function () {
					if ($(window).scrollTop()>100) {
						$(".tagline").fadeTo( "slow", 0 );
					}
					else {
						$(".tagline").fadeTo( "slow", 1 );
					}
			    };
					
			});
			</script>


</body>
</html>































<!-- <!DOCTYPE html>
<html>
<head>
	<title>Detail Musik</title>
</head>
<body>

<?php 
	$id = $_GET['id'];

			$sql_nama_album = "Select nama_album, id from t_album where id=$id";
			$result_nama_album = mysqli_query($link, $sql_nama_album);

			$sql_tampil = "SELECT judul_lagu, penyanyi, file_src, durasi, id FROM t_lagu WHERE id_album = $id";

			$result = mysqli_query($link, $sql_tampil);

	 		$banyakrecord = mysqli_num_rows($result);

	 		if($banyakrecord>0){ ?>
				<div align="center">Data Kategori ditemukan sebanyak : <b><?php echo $banyakrecord;?> </b>Record</div> 
				
				<table id="example" style="font-size: medium; margin-left: 0px;">
					<thead style="background-color: #0066FF; color: white;">
						<tr>				
							<th align="center">Penyanyi</th>
							<th align="center">Judul</th>
							<th align="center">Musik</th>
							<th align="center">Durasi</th>
							<th></th>
							<th align="center"> Aksi </th>

						</tr> 
					</thead>
					<tbody>
						<?php
						$i=0; 
						while( $data=mysqli_fetch_array($result)){ 
						$i++;
						 ?>  
						 <tr>
							  <td align="center"><?php echo $data['penyanyi'];?></td> 
							  <td align="center"><?php echo $data['judul_lagu'];?></td>
							  <td align="center"><?php echo $data['file_src'];?></td>
							  <td align="center"><?php echo $data['durasi'];?></td>
							  <td>
							  		<audio controls>
									  <source src="<?php echo "mp3/".$data ['file_src']; ?>" type="audio/mpeg">
									</audio>
							  </td>
							  <td><a href ="proseshapus_detail.php?id=<?php echo $data['id'];?>"><button type="button" class="btn btn-primary">Hapus</button></a></td>
						  </tr>
						  <?php
						  
						  } 
						  ?>
						  </tbody>
						   </table> 
						   <?php
						   } else echo "Tidak ada data pada tabel Kategori."; 
						   ?>
						
						<div class="modal-footer">
					          <a href ="tampildata.php"><button type="button" class="btn btn-secondary" data-dismiss="modal">Selesai</button></a>
				        </div>

</form>


</body>
</html>
-->