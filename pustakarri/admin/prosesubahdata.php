<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title>Admin</title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">

			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">
			
			<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
			<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
			<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>

		<body>
		<!-- Header -->
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#topWrap">
							<span class="fa-stack fa-lg">
								
							</span>
							<span>
							<img src="themes/image/we.png" ><font size="+2" font color="#0066FF"> Pustaka Siar RRI Bandung </font>
							</span>
						</a>
					</div>
					<div class="collapse navbar-collapse appiNav">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Beranda</a></li>
							<li><a href="#productWrap">Lagu</a></li>
							<li><a href="#pricingWrap">Artis</a></li>
							<li><a href="#testimonialsWrap">Album</a></li>
							<li><a href="#contactWrap">Logout</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
				<br>
			</div>
			<!-- // Header -->
<br>
<br>
<br>
<br>
<br>
<br />
<br />
	
	<p align="center" >UBAH DATA</p>
	<?php

	function koneksi_db(){
	$host = "localhost";
	$database = "db_pustaka_siaran";
	$user = "root";
	$password = "";
	$link = mysqli_connect($host,$user,$password);
	mysqli_select_db($link,$database);
	if(!$link){
		echo "Error : ".mysqli_error($link);
		}
	return $link;
}
?> 

<?php
	$kode_konten_umum=$_POST['kode_konten_umum'];//$_POST['K_umum'];
	$kode_sumber_data=$_POST['kode_sumber_data'];
	$jenis_kategori_musik=$_POST['kode_jenis_musik'];
	$fungsi_jenis_musik=$_POST['id_detail_fungsi_musik'];
	$detail_fungsi_jenis_musik=$_POST['kode'];
	$kode_vocal_instument=$_POST['kode_vocal_intrument'];
	$no_urut=$_POST['no_urut'];
	$link=koneksi(); 
	$sql="	UPDATE SET  
			t_perpus_lagu
			INNER JOIN m_detail_fungsi_musik ON m_detail_fungsi_musik.id = t_perpus_lagu.id_detail_fungsi_musik
			INNER JOIN t_album ON t_album.id = t_perpus_lagu.id_album
			LEFT JOIN t_lagu ON t_lagu.id_album = t_album.id
			set
			t_perpus_lagu.kode_sumber_data='kode_sumber_data',t_perpus_lagu.jenis_kategori_musik='jenis_kategori_musik',t_perpus_lagu.fungsi_jenis_musik='fungsi_jenis_musik',t_perpus_lagu.detail_fungsi_jenis_musik='detail_fungsi_jenis_musik',t_perpus_lagu.kode_vocal_instument='kode_vocal_instument',t_perpus_lagu.no_urut='no_urut' where t_perpus_lagu.kode_konten_umum='kode_konten_umum'
			"; 
	//var_dump($sql);
	$res= mysqli_query($link,$sql); 
	if(mysqli_num_rows($res)==1){ 
	$data= mysqli_fetch_array($res); 
	//var_dump($data);
	?> 
	<form method=post action="prosestambah.php" class="form-horizontal" enctype="multipart/form-data" >

	  		<!-- Pilih konten umum -->
	  		<div class="form-group">
				<label for="konten_umum" class="col-sm-4 control-label">Konten Umum</label>
				<div class="col-sm-8">
					<select id="konten_umum" name="konten_umum" class="form-control">
						<option value="<?php echo $data['kode_konten_umum'] ?>">Pilih Konten Umum</option>
						<?php 
							while ($row = mysqli_fetch_array($result_konten_umum)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>

			<!-- Pilih sumber data -->
			<div class="form-group">
				<label for="Sumber_data" class="col-sm-4 control-label">Sumber Data</label>
				<div class="col-sm-8">
					<select onChange="ambil_form_asing()" id="sumber_data" name="sumber_data" class="form-control">
						<option value="<?php echo $data['kode_sumber_data'] ?>">Pilih Sumber Data</option>
						<?php 
							while ($row = mysqli_fetch_array($result_sumber_data)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>
			<div id="tmp_form_asing"></div>

			<!-- Pilih Jenis kategori-->
			<div class="form-group">
				<label for="kategori_musik" class="col-sm-4 control-label">Jenis Kategori Musik</label>
				<div class="col-sm-8">
					<select onChange="ambil_form_vocal_instrumen()" id="kategori_musik" name="kategori_musik" class="form-control">
						<option value="<?php echo $data['kode_jenis_musik'] ?>">Jenis Kategori Musik</option>
						<?php 
							while ($row = mysqli_fetch_array($result_jenis_musik)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>
			<div id="tmp_form_vocal"></div>

			<!-- Pilih Fungsi Musik -->
			<div class="form-group">
				<label for="fungsi_musik" class="col-sm-4 control-label">Fungsi Jenis Musik</label>
				<div class="col-sm-8">
					<select onChange="ambil_fungsi_musik()" id="fungsi_musik" name="fungsi_musik" class="form-control">
						<option value="<?php echo $data['id_detail_fungsi_musik'] ?>">Fungsi Jenis Musik</option>
						<?php 
							while ($row = mysqli_fetch_array($result_fungsi_musik)) {
								echo "<option value='".$row['kode']."'>".$row['text']."</option>"; 
							}
						?>
					</select>
				</div>
			</div>

			<!--  Detail Fungsi Musik-->
			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Detail Fungsi Jenis Musik</label>
				<div class="col-sm-8">
					<select id="detail_musik" class="form-control" name="detail_musik">
						<option value="<?php echo $data['kode'] ?>">Detail Fungsi Jenis Musik</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">Vocal</label>
				<div class="col-sm-8">
					<select id="detail_musik" class="form-control" name="detail_musik">
						<option value="<?php echo $data['kode_vocal_intrument'] ?>">Detail Fungsi Jenis Musik</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="detail_musik" class="col-sm-4 control-label">No urut</label>
				<div class="col-sm-8">
					<select id="detail_musik" class="form-control" name="detail_musik">
						<option value="<?php echo $data['no_urut'] ?>">No Urut</option>
					</select>
				</div>
			</div>
			




	
	
			<?php include "nav/footer.php" ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.js"></script>
	
<script type="text/javascript">
$(document).ready(function() {

	$('.mobileSlider').flexslider({
		animation: "slide",
		slideshowSpeed: 3000,
		controlNav: false,
		directionNav: true,
		prevText: "&#171;",
		nextText: "&#187;"
	});
	$('.flexslider').flexslider({
		animation: "slide",
		directionNav: false
	});
		
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if ($(window).width() < 768) {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar-header').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}
			else {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}

		}
	});
	
	$('#toTop').click(function() {
		$('html,body').animate({
			scrollTop: 0
		}, 1000);
	});
	
	var timer;
    $(window).bind('scroll',function () {
        clearTimeout(timer);
        timer = setTimeout( refresh , 50 );
    });
    var refresh = function () {
		if ($(window).scrollTop()>100) {
			$(".tagline").fadeTo( "slow", 0 );
		}
		else {
			$(".tagline").fadeTo( "slow", 1 );
		}
    };
		
});
</script>
  </body>
</html>