<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title>Admin</title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">

			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">
			
			<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
			<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
			<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>

		<body>
		<!-- Header -->
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#topWrap">
							<span class="fa-stack fa-lg">
								
							</span>
							<span>
							<img src="themes/image/we.png" ><font size="+2" font color="#0066FF">  Pustaka Siaran RRI Bandung  </font>
							</span>
						</a>
					</div>
					<div class="collapse navbar-collapse appiNav">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Beranda</a></li>
							<li><a href="#productWrap">Lagu</a></li>
							<li><a href="#pricingWrap">Artis</a></li>
							<li><a href="#testimonialsWrap">Album</a></li>
							<li><a href="#contactWrap">Logout</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
				<br>
			</div>
			<!-- // Header -->
			
<br>
<br>
<br>
<br>
<br>

<?php
	include('koneksi/koneksi.php');
	$link = koneksi_db();
	$id_album = $_GET['id_album'];
	$sql = ' SELECT CONCAT(
	t_album.nama_album, ".",
	t_album.tahun_album, ".",
	t_album.penyanyi, ".",
    t_album.file_src, ".",
    t_lagu.judul_lagu, ".",
	IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
	(SELECT m_jenis_vocal.kode FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
	(SELECT m_jenis_instrument.kode FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)), ".",
	t_perpus_lagu.no_urut
) AS kodedetail
			
		FROM
			t_perpus_lagu
			INNER JOIN m_detail_fungsi_musik ON m_detail_fungsi_musik.id = t_perpus_lagu.id_detail_fungsi_musik
			INNER JOIN t_album ON t_album.id = t_perpus_lagu.id_album
			LEFT JOIN t_lagu ON t_lagu.id_album = t_album.id

	WHERE
		t_album.id = '.$id_album.'
		'; 
 
$res=mysqli_query($link,$sql);
$data=mysqli_fetch_row($res);
//var_dump ($sql);
			 
?> 

		 <div class="row" style="margin-top:50px">
  		<div class="col-md-3"></div>
 	 <div class="col-md-6">
  		<h2 align="center">Deskripsi</h2>
		</div>
		</div>
		
	 <input type="hidden" name="id_album" value="<?php echo $data['id_album'];?>">
	<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-push-8 col">
        <table>
		<tr>
		<th>Gambar	</th>
		<th><input type="text" class="form-control" id="file_src" name="file_src" value="<?php echo $data['file_src'];?> "></th>
		</tr>
		<tr>
		<th>Judul Lagu</th>
		<th><input type="text" class="form-control" id="judul_lagu" name="judul_lagu" value="<?php echo $data['judul_lagu'];?> "></th>
		</tr></tr></table>
        </div>
        <div class="col-md-8 col-md-pull-4 col">
         <table><tr>
		<th>Nama Album 	</th>
		<th><input type="text" class="form-control" id="nama_album" name="nama_album" value="<?php echo $data['nama_album'];?> "></th>
		</tr></tr> 
		<tr>
		<th>Tahun	</th>
		<th><input type="text" class="form-control" id="tahun_album" name="tahun_album" value="<?php echo $data['tahun_album'];?> "></th>
		</tr></tr> 
		<tr>
		<th>Nama Penyanyi </th>
		<th><input type="text" class="form-control" id="penyanyi" name="penyanyi" value="<?php echo $data['penyanyi'];?> "></th>
		</tr></tr></table> 
        </div>
    </div>
</div>	


   
<?php include "nav/footer.php" ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.js"></script>
	
<script type="text/javascript">

$(document).ready(function() {

	//$('select[name=vocal_instrumen]').val('');
	//$('select[name=detail_musik]').val('');
	
	//$('.selectpicker').selectpicker('refresh')


	$('.mobileSlider').flexslider({
		animation: "slide",
		slideshowSpeed: 3000,
		controlNav: false,
		directionNav: true,
		prevText: "&#171;",
		nextText: "&#187;"
	});
	$('.flexslider').flexslider({
		animation: "slide",
		directionNav: false
	});
		
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if ($(window).width() < 768) {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar-header').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}
			else {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}

		}
	});
	
	$('#toTop').click(function() {
		$('html,body').animate({
			scrollTop: 0
		}, 1000);
	});
	
	var timer;
    $(window).bind('scroll',function () {
        clearTimeout(timer);
        timer = setTimeout( refresh , 50 );
    });
    var refresh = function () {
		if ($(window).scrollTop()>100) {
			$(".tagline").fadeTo( "slow", 0 );
		}
		else {
			$(".tagline").fadeTo( "slow", 1 );
		}
    };

    
    ambil_form_vocal_instrumen('<?php echo $data_row['kode_vocal_intrument'] ?>','<?php echo $data_row['id_detail_musik'] ?>');
   
});
 function ambil_fungsi_musik()
  {
      $("#detail_musik").empty();
      var value = $("#fungsi_musik").val();
      $.post('ambil_fungsi_musik.php', {
            id: value,
            //kode:kode
      }, function(data, status){
          //alert(data);
          $("#detail_musik").html(data);

      });
  }

  
  function ambil_form_vocal_instrumen(kode, kode_detail)
  {
      $("#tmp_form_vocal").empty();
      var value = $("#kategori_musik").val();
      $.post('ambil_form_vocal_instrumen.php', {
            id: value,
            kode: kode,
            kode_detail: kode_detail
      }, function(data, status){
          //alert(data);
          $("#tmp_form_vocal").html(data);

      });
  }

  function ambil_form_asing()
  {
  	$("#tmp_form_asing").empty();
      var value = $("#sumber_data").val();
      $.post('ambil_form_asing.php', {
            id: value
      }, function(data, status){
          //alert(data);
          $("#tmp_form_asing").html(data);

      });
  }


</script>
  </body>
</html>