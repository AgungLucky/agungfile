<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title> Pustaka Siaran RRI Bandung </title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">

			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">

			
			<link href="assets/fa/css/font-awesome.css" rel="stylesheet">
			<link href='assets/font-google/font1.css' rel='stylesheet' type='text/css'>
			<link href='assets/font-google2/font2.css' rel='stylesheet' type='text/css'>

			<!-- CSS Header -->
			<!-- jQuery library (served from Google) -->
			<script src="js/jquery.min.js"></script>
			<!-- bxSlider Javascript file -->
			<script src="slideshow/jquery.bxslider.min.js"></script>
			<!-- bxSlider CSS file -->
			<link href="slideshow/jquery.bxslider.css" rel="stylesheet">

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->

			<script>
			$(document).ready(function(){
				  $('.bxSlider').bxSlider({
						  auto: true,
						});
			});
			</script>

		</head>

		<body>
		
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php">
							<span class="fa-stack fa-lg">
								
							</span>
							<span>
							<img src="themes/image/we.png" ><font size="+2" font color="#0088b0">  Pustaka Siaran RRI Bandung  </font>
							</span>
						</a>
					</div>
					<ul class="nav navbar-nav">
							<li><a href="index.php">Beranda</a></li>
							<li><a href="tampilalbum.php">Pustaka Siaran RRI</a></li>
							<li><a href="programa.php">Pro RRI</a></li>
							
						</ul>
				</div><!--/.nav-collapse -->
				<br>
			</div>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
			<div class="row" style="margin-top:50px">
			  <div class="col-md-1"></div>	
			  <div class="col-md-5">
			  	<h1 align="center">Pro 1 RRI Bandung</h1>
			  			<details>
							 	<summary><center><img src="gambar/pro01.png" width="300px" height="300px"></center></summary>
							 	<br>
							 	<p style="text-align:justify;">
							 		PRO 1 adalah saluran radio milik RRI (Radio Republik Indonesia), jaringan radio pemerintah, dimiliki dan dikelola oleh Pemerintah Indonesia. pemrograman termasuk Informasi, Pendidikan, Hiburan dan Kebudayaan.
							 	</p>
							 	<p style="text-align:justify;">
							 		Main Programs :
							 		<ul>
										 <li>Kuliah Subuh</li>
										 <li>Harmoni Pagi</li>
										 <li>Zona Edukasi</li>
										 <li>Zona Kontemplasi</li>
										 <li>Pro 1 RRi Nuansa Malam</li>
									</ul>
								</p>
						</details> 

						<h1 align="center">Pro 3 RRI Bandung</h1>
			  			<details>
							 	<summary><center><img src="gambar/pro3.jpg" width="300px" height="300px"></center></summary>
							 	<br>
							 	<p style="text-align:justify;">
							 		Pusat Pemberitaan RRI PRO3 FM 88.8 MHz AM 999 KHz
							 		Acara Unggulan RRI PRO3 <br>
							 		<ul>
										<li>Aspirasi Merah Putih</li>
										<li>Sambung Rasa</li>
										<li>Lintas Berita,Indonesia Menyapa dan lain-lain</li>
							 		</ul>
							 	</p>
							 	<p style="text-align:justify;">
								
								</p>
						</details> 	
			  </div>
			  <div class="col-md-5">
			  	<h1 align="center">Pro 2 RRI Bandung</h1>
			  			<details>
							 	<summary><center><img src="gambar/pro02.png" width="300px" height="300px"></center></summary>
							 	<br>
							 	<p style="text-align:justify; margin-left:20px">
							 		Sebuah Bagian Dari Lembaga Penyiaran Publik [LPP] RRI Bandung. Sejak Tahun 2003, Programa 2 management-nya dikelola langsung oleh RRI setelah sebelumnya di KSO [Kerjasama Operasional]-kan oleh Pihak Swasta.Pro 2 RRI Bandung berada pada FM 96 MHz.
							 	<p style="text-align:justify; margin-left:20px">
									Profile :<br>
									<ul>	
										<li> S.E.S         : 17 - 35 Tahun [Pedoman RRI]</li>
										<li> Audience Call : Pendengar Pro2</li>
										<li> Music         : 50 % [Indonesia] - 50% [Mancanegara]</li>
									</ul>
								</p>
						</details> 

						<h1 align="center">Pro 4 RRI Bandung</h1>
			  			<details>
							 	<summary><center><img src="gambar/pro4.jpg" width="300px" height="300px"></center></summary>
							 	<br>
							 	<p style="text-align:justify; margin-left:20px">
							 		Pro 4 RRI Bandung merupakan kanal siaran yang menyuguhkan program seni dan budaya daerah,
									khususnya seni dan budaya jawa barat.Program unggulannya adalah : Rampak ngawangun,Gelar Kawih Pasundan dan Heuheuy Deudeuh.
							 	<p style="text-align:justify;">
									
								</p>
						</details> 
			  </div>
			  <div class="col-md-1"></div>
			</div>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
			
		
				
	
	
	<?php include "nav/footer.php" ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
 
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.js"></script>
		<!--	<details>
			  <summary>Copyright 1999-2014.</summary>
			  <p> - by Refsnes Data. All Rights Reserved.</p>
			  <p>All content and graphics on this web site are the property of the company Refsnes Data.</p>
			</details> 
			
	<div class="body">
		<div>
			<div>
				<div>
					<div class="about">
						<div>
							<h2 align="center">Programa RRI BANDUNG</h2>

					<div>
						<img src="gambar/pro01.png" alt="Image">
						</div>
							<p align="justify">
							<h1>Pro 1 RRI Bandung</h1>
								<p align="justify">PRO 1 adalah saluran radio milik RRI (Radio Republik Indonesia), jaringan radio pemerintah, dimiliki dan dikelola oleh Pemerintah Indonesia. pemrograman termasuk Informasi, Pendidikan, Hiburan dan Kebudayaan.</p>
							</p>
							<p align="justify">
							Main Programs : Kuliah Subuh,Harmoni Pagi,<br>Zona Edukasi<br>Zona Kontemplasi <br>Pro 1 Rri Nuansa Malam
							</p>
						
										
					
					<div>
						<img src="gambar/pro02.png" alt="Image">
						</div>
							<p align="justify">
							<h1>Pro 2 RRI Bandung</h1>
								<p align="justify">Sebuah Bagian Dari Lembaga Penyiaran Publik [LPP] RRI Bandung. Sejak Tahun 2003, Programa 2 management-nya dikelola langsung oleh RRI setelah sebelumnya di KSO [Kerjasama Operasional]-kan oleh Pihak Swasta.Pro 2 RRI Bandung berada pada FM 96 MHz.
							</p>
							<p align="justify">
							Profile :<br>
							S.E.S : 17 � 35 Tahun [Pedoman RRI]<br>
							Audience Call : Pendengar Pro2<br>
							Music : 50 % [Indonesia] � 50% [Mancanegara]
							</p>
							
							<div>
						<img src="gambar/pro3.jpg" alt="Image">
						</div>
							<p align="justify">
							<h1>Pro 3 RRI Bandung</h1>
								<p align="justify">Pusat Pemberitaan RRI PRO3 FM 88.8 MHz AM 999 KHz<br>
								Acara Unggulan RRI PRO3: <br>
								- Aspirasi Merah Putih <br>
								- Sambung Rasa <br>
								- Lintas Berita,Indonesia Menyapa dan lain-lain.
								
							</p>
							
							<div>
						<img src="gambar/pro4.jpg" alt="Image">
						</div>
							<p align="justify">
							<h1>Pro 4 RRI Bandung</h1>
								<p align="justify">Pro 4 RRI Bandung merupakan kanal siaran yang menyuguhkan program seni dan budaya daerah,
								khususnya seni dan budaya jawa barat.Program unggulannya adalah : Rampak ngawangun,Gelar Kawih Pasundan dan Heuheuy Deudeuh.
								
							</p>
							
							
						
					
			
	</div>


								
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	-->

		
</body>
</html>