<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title>Tampil Musik</title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">
			<link href="css/bootstrap-table.css" rel="stylesheet">
		
			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">
			
			
			
			
			<link href="assets/fa/css/font-awesome.css" rel="stylesheet">
			<link href='assets/font-google/font1.css' rel='stylesheet' type='text/css'>
			<link href='assets/font-google2/font2.css' rel='stylesheet' type='text/css'>

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->

</head>
<body>
<!-- Header -->
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php">
							<span class="fa-stack fa-lg">
								
							</span>
							<span>
							<img src="themes/image/we.png" ><font size="+2" font color="#0088b0"> Pustaka Siaran RRI Bandung </font>
							</span>
						</a>
					</div>
						<ul class="nav navbar-nav">
							<li><a href="index.php">Beranda</a></li>
							<li><a href="tampilalbum.php">Pustaka Siaran RRI</a></li>
							<li><a href="programa.php">Pro RRI</a></li>
							
						</ul>
				</div><!--/.nav-collapse -->
				<br>
			</div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<!-- UJI COBA PENCARIAN -->
<div class="col-md-3"></div>
<div class="col-md-6">
<form method="post" action="<?php $_SERVER[ 'PHP_SELF' ]."?tampilalbum"?>" >

	<h2 align="center">Pencarian</h2>
	<br>
<!--	<div class="col-sm-4">
		<select name="fieldcari" class="form-control">
			<option value="nama_album" <?php if(isset($_POST['fieldcari']) and $_POST['fieldcari']=="nama_album") echo "selected";?>>Nama Album</option>
		    <option value="judul_lagu" <?php if(isset($_POST['fieldcari']) and $_POST['fieldcari']=="judul_lagu") echo "selected";?>>Judul Lagu</option>
		    <option value="t_lagu.penyanyi" <?php if(isset($_POST['fieldcari']) and $_POST['fieldcari']=="penyanyi") echo "selected";?>>Penyanyi</option>
		   
		</select>
	</div> 
	<div class="col-sm-6">
	<input type="text" class="form-control" name="keyword" size=10 maxlength="40" value="<?php isset($_POST [ 'keyword' ]) ? $_POST [ 'keyword' ] : "" ;?>">
	</div>
	<input type="submit" class="btn btn-info pull-right" name="btncari" value="Cari">
</form> -->
<?php
	include('koneksi/koneksi.php');
	$link = koneksi_db();
	
	/*	$sql = "
				SELECT t_album.id, t_album.nama_album,t_lagu.id_album, t_lagu.id, t_lagu.judul_lagu, t_lagu.penyanyi, t_lagu.file_src
				FROM t_album JOIN t_lagu ON t_album.id = t_lagu.id_album
					
			";
	*/
	

	$sql = '
			SELECT DISTINCT
t_perpus_lagu.kode_konten_umum, t_perpus_lagu.kode_sumber_data, t_perpus_lagu.kode_jenis_musik, m_detail_fungsi_musik.kode_fungsi_musik, m_detail_fungsi_musik.kode,t_album.id, t_album.nama_album,t_lagu.id_album, t_lagu.id as idlagu, t_lagu.judul_lagu, t_lagu.penyanyi, t_lagu.file_src, IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
			(SELECT m_jenis_vocal.kode FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
			(SELECT m_jenis_instrument.kode FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)) as kode_vocal_intrument,
			IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
			(SELECT m_jenis_vocal.text FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
			(SELECT m_jenis_instrument.text FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)) as text_vocal_intrument,
			t_perpus_lagu.no_urut,
			t_album.nama_album, t_perpus_lagu.id,

	CONCAT(
	t_perpus_lagu.kode_konten_umum, ".",
	t_perpus_lagu.kode_sumber_data, ".",
	t_perpus_lagu.kode_jenis_musik, ".",
	m_detail_fungsi_musik.kode_fungsi_musik, ".",
	m_detail_fungsi_musik.kode, ".",
	IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
	(SELECT m_jenis_vocal.kode FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
	(SELECT m_jenis_instrument.kode FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)), ".",
	t_perpus_lagu.no_urut
) AS kodedetail
			
		FROM
			t_perpus_lagu
			INNER JOIN m_detail_fungsi_musik ON m_detail_fungsi_musik.id = t_perpus_lagu.id_detail_fungsi_musik
			INNER JOIN t_album ON t_album.id = t_perpus_lagu.id_album
			LEFT JOIN t_lagu ON t_lagu.id_album = t_album.id

			'; 

	$fieldcari = isset($_POST [ 'fieldcari' ]) ? $_POST [ 'fieldcari' ] : "" ;
	$keyword = isset($_POST [ 'keyword' ]) ? $_POST [ 'keyword' ] : "" ;

	if( isset($_POST [ 'btncari' ]) and $_POST['btncari'] == "Cari" ) // Jika tblcari diklik, tambahkan perintah WHERE ...
		$sql = $sql.  " where $fieldcari like '%$keyword%' " ;
		$sql .=  " order by judul_lagu" ; 
//var_dump($sql);
	$res = mysqli_query($link, $sql);
	$banyakrecord = mysqli_num_rows($res);
	if( $banyakrecord > 0 ){


			?>
<!--
<br>
<br>
<br>
<br>
-->
	<div class="panel panel-default">
	<div class="panel-heading"></div>
	<div class="panel-body">
	
	<table data-toggle="table" data-url="t-body.php"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
				<thead style="background-color: #0088b0; color: white;">
				<tr>
					<th><center>Kode Album</center></th>
					<th><center>Nama Album</center></th>
					<th><center>Judul Lagu</center></th>
					<th><center>Penyanyi</center></th>
					<th><center>Aksi</center></th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 0 ;
				while( $data = mysqli_fetch_array($res)){
				$i ++;
				?>
			<tr>	
				<td align="center"> <?php echo $data [ 'kodedetail' ]; ?> </td>
				<td align="center"> <?php echo $data [ 'nama_album' ]; ?> </td>
				<td align="center"> <?php echo $data [ 'judul_lagu' ]; ?> </td>
				<td align="center"> <?php echo $data [ 'penyanyi' ]; ?> </td>
				<td align="center"><a href="detailalbum.php?id=<?php echo $data['id_album'];?>&link=<?php echo $data['idlagu']; ?> "><button type="button" class="btn btn-info pull-right">Detail</button></a></td>
			</tr>
				<?php
				}
				?>
			</tbody>
		</table> 
	</div>
	</div>
	<?php
	}else {
	?>
	<br><br><p align="center" style="font-size:19px">Data Musik tidak ditemukan </p>
	<?php
	}
	?>

	
</div>
</div>
</div><!-- TUTUP DIV COL-MD -->


<div class="col-md-3"></div>
</div>
</div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<?php include "nav/footer.php" ?>

<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->\
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.js"></script>
	<script src="js/bootstrap-table.js"></script>


<script type="text/javascript">

    
     $(document).ready(function() {
    $('#example').DataTable();
    });
  
$(document).ready(function() {

	$('.mobileSlider').flexslider({
		animation: "slide",
		slideshowSpeed: 3000,
		controlNav: false,
		directionNav: true,
		prevText: "&#171;",
		nextText: "&#187;"
	});
	$('.flexslider').flexslider({
		animation: "slide",
		directionNav: false
	});
		
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if ($(window).width() < 768) {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar-header').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}
			else {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}

		}
	});
	
	$('#toTop').click(function() {
		$('html,body').animate({
			scrollTop: 0
		}, 1000);
	});
	
	var timer;
    $(window).bind('scroll',function () {
        clearTimeout(timer);
        timer = setTimeout( refresh , 50 );
    });
    var refresh = function () {
		if ($(window).scrollTop()>100) {
			$(".tagline").fadeTo( "slow", 0 );
		}
		else {
			$(".tagline").fadeTo( "slow", 1 );
		}
    };
		
});
</script>

</body>
</html>