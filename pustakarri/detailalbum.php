<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title>Detail Album</title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">
			<link href="css/data_table.css" rel="stylesheet">
			<link href="admin/css/playlist.css" rel="stylesheet">

			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">
			
			<link href="assets/fa/css/font-awesome.css" rel="stylesheet">
			<link href='assets/font-google/font1.css' rel='stylesheet' type='text/css'>
			<link href='assets/font-google2/font2.css' rel='stylesheet' type='text/css'>
			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>

		<body>
		<br>
		<a href="tampilalbum.php"><img src="images/logo rri.jpg" width="375 px" height="75 px" style="margin-left : 40px"></a>
	<hr>
	<div class="row" style="margin-top:2px">
    	<div class="col-md-1"></div>
    	<div class="col-md-10">

<?php
	$id_perpus_lagu = $_GET['id'];
	$link_id = $_GET['link'];
	include('koneksi/koneksi.php');
	$link = koneksi_db(); 
	
	$sql_konten_umum = "Select * from m_konten_umum";
	$result_konten_umum = mysqli_query($link, $sql_konten_umum);

	$sql_sumber_data = "Select * from m_sumber_data";
	$result_sumber_data = mysqli_query($link, $sql_sumber_data);

	$sql_jenis_musik = "Select * from m_jenis_musik";
	$result_jenis_musik = mysqli_query($link, $sql_jenis_musik);

	$sql_fungsi_musik = "Select * from m_fungsi_musik";
	$result_fungsi_musik = mysqli_query($link, $sql_fungsi_musik);

	$sql_detail_fungsi_musik = "Select * from m_detail_fungsi_musik";
	$result_detail_fungsi_musik = mysqli_query($link, $sql_detail_fungsi_musik);



		$sql_ambil_data = '
	SELECT DISTINCT
t_perpus_lagu.kode_konten_umum, t_perpus_lagu.kode_sumber_data, t_perpus_lagu.kode_jenis_musik, m_detail_fungsi_musik.kode_fungsi_musik, m_detail_fungsi_musik.kode, 
m_detail_fungsi_musik.id as id_detail_musik,
IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
			(SELECT m_jenis_vocal.kode FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
			(SELECT m_jenis_instrument.kode FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)) as kode_vocal_intrument,
			IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
			(SELECT m_jenis_vocal.text FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
			(SELECT m_jenis_instrument.text FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)) as text_vocal_intrument,
			t_perpus_lagu.no_urut,
			t_perpus_lagu.id_jenis_penyimpanan,
			t_album.nama_album, t_perpus_lagu.id, t_album.tahun_album, t_album.deskripsi, t_album.id as id_album, 
	t_perpus_lagu.id as id_perpus_lagu, t_album.file_src, t_lagu.judul_lagu, t_lagu.file_src as file_lagu,

	CONCAT(
	t_perpus_lagu.kode_konten_umum, ".",
	t_perpus_lagu.kode_sumber_data, ".",
	t_perpus_lagu.kode_jenis_musik, ".",
	m_detail_fungsi_musik.kode_fungsi_musik, ".",
	m_detail_fungsi_musik.kode, ".",
	IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
	(SELECT m_jenis_vocal.kode FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
	(SELECT m_jenis_instrument.kode FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)), ".",
	t_perpus_lagu.no_urut
) AS kodedetail
			
		FROM
			t_perpus_lagu
			INNER JOIN m_detail_fungsi_musik ON m_detail_fungsi_musik.id = t_perpus_lagu.id_detail_fungsi_musik
			INNER JOIN t_album ON t_album.id = t_perpus_lagu.id_album
			LEFT JOIN t_lagu ON t_lagu.id_album = t_album.id

	WHERE
		t_perpus_lagu.id_album = '.$id_perpus_lagu.' AND t_lagu.id = '.$link_id.'
 '; 

 $result_ambil_data = mysqli_query($link,$sql_ambil_data);

 $data_row = mysqli_fetch_array($result_ambil_data);




$sql_ambil_gambar = "SELECT file_src FROM t_album  WHERE id = $id_perpus_lagu";

$hasil = mysqli_query($link,$sql_ambil_gambar);
$res = mysqli_fetch_array($hasil);




?>

  		<h2 align="center">Detail Album</h2>

  		<form class="form-horizontal" enctype="multipart/form-data" >

  			<input type="hidden" name="id_album" value="<?php echo $link_id;?>">
  			<input type="hidden" name="id_perpus_lagu" value="<?php echo $id;?>">

  		<table border="0" align="center">
				<tr>
					<td style="font-size:17px" width="200px" align=""><b>Nama Album<b></td>
					<td style="font-size:17px" width="200px" align=""><?php  echo $data_row['nama_album'];?></td>
					<td width="300px" rowspan="3" align="center"><img src="admin/<?php echo $res['file_src'];?>" class="img-rounded" width="250px" height="250px"></td>

				</tr>
				<tr>
					<td style="font-size:17px" width="200px" align=""><b>Tahun Rilis<b></td>
					<td style="font-size:17px" width="200px" align=""><?php  echo $data_row['tahun_album'];?></td>
				</tr>
				<tr>
					<td style="font-size:17px" width="200px" align=""><b>Deskripsi<b></td>
					<td style="font-size:17px" width="900px" align=""><p align="justify"><?php  echo $data_row['deskripsi'];?></p></td>
				</tr>
				<tr>
					<td style="font-size:17px" width="200px" align=""><b>Judul Lagu<b></td>
					<td style="font-size:17px" width="900px" align=""><p align="justify"><?php  echo $data_row['judul_lagu'];?></p></td>
				</tr>
			</table>
<pre>
		<audio controls><source src="<?php echo "admin/mp3/".$data_row ['file_lagu']; ?>" type="audio/mpeg"></audio>
</pre>
<br>


					
			
				

<!-- Ambil Lagu -->
<details>
 <summary><button type="button" class="btn btn-info">Lihat Playlist</button></summary>
<?php

	$sql_ambil_judul = 
	"SELECT t_lagu.judul_lagu, t_lagu.penyanyi, t_album.file_src, t_lagu.durasi, t_lagu.file_src as file_musik FROM t_lagu JOIN t_album ON t_lagu.id_album = t_album.id WHERE t_album.id  = $id_perpus_lagu";

	$sql_judul = mysqli_query($link,$sql_ambil_judul);
	$result_judul = mysqli_fetch_array($sql_judul);

	$res=mysqli_query($link,$sql_ambil_judul); 
	$banyakrecord=mysqli_num_rows($res); 
		if($banyakrecord>0){ ?> 

			<div class="container">
				    <div class="column center">
				        <center><h3> Daftar Musik </h3></center>
				    </div>
				    <div class="column add-bottom">
				        <div id="mainwrap">
				            <div id="nowPlay">
				                <span class="left" id="npAction"></span>
				                <span class="right" id="npTitle"></span>
				            </div>
				            <div id="audiowrap">
				                <div id="audio0">
				                    <audio preload id="audio1" controls="controls">Your browser does not support HTML5 Audio!</audio>
				                </div>
				                <div id="tracks">
				                    <a id="btnPrev">&laquo;</a>
				                    <a id="btnNext">&raquo;</a>
				                </div>
				            </div>
				            <div id="plwrap">
				                <ul id="plList">
				                <?php
				                $i=0; 
								$arr_lagu = array();
								while( $data=mysqli_fetch_array($res)){ 
									$lagu = explode(".", $data['file_musik']);
									array_pop($lagu);
									$lagu = implode(".", $lagu);
										$arr_lagu[$i] = array(
												'track' => $i+1,
												'name' => $data['judul_lagu'],
												'length' => $data['durasi'],
												'file' => $lagu 
										);


										?>

							
				                    <li>
				                        <div class="plItem">
				                            <div class="plNum"><?php echo $i+1; ?></div>
				                            <div class="plTitle"><?php echo $data['judul_lagu'];?></div>
				                            <div class="plLength"><?php echo $data['durasi'];?></div>
				                        </div>
				                    </li>

				                    <?php

				                    	$i++;
							}
							?>

				                </ul>
				            </div>
				        </div>
				    </div>
				</div>
		   <?php
		   		} else {
		   			echo "Tidak ada data pada tabel Kategori.";	
		   		}  
		   ?>

		   	

  		</form>
<br>

  </div>
</div>
<?php 
$json_array_lagu = json_encode($arr_lagu);
?>
</details>
<br>
  			<a href="tampilalbum.php"><button type="button" class="btn btn-info" style="margin-left:1000px">Kembali</button></a>
			<br>
<div class="col-md-1"></div>
<br>
 



	
			<?php include "nav/footer.php" ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.js"></script>
    <script src="js/data_table.js"></script>


<script type="text/javascript">
// html5media enables <video> and <audio> tags in all major browsers
// External File: http://api.html5media.info/1.1.8/html5media.min.js


// Add user agent as an attribute on the <html> tag...
// Inspiration: http://css-tricks.com/ie-10-specific-styles/
var b = document.documentElement;
b.setAttribute('data-useragent', navigator.userAgent);
b.setAttribute('data-platform', navigator.platform);


// HTML5 audio player + playlist controls...
// Inspiration: http://jonhall.info/how_to/create_a_playlist_for_html5_audio
// Mythium Archive: https://archive.org/details/mythium/
jQuery(function ($) {
    var supportsAudio = !!document.createElement('audio').canPlayType;
    if (supportsAudio) {
        var index = 0,
            playing = false,
            mediaPath = 'admin/mp3/',
            extension = '',
            tracks =<?php echo $json_array_lagu ?>,
            trackCount = tracks.length,
            npAction = $('#npAction'),
            npTitle = $('#npTitle'),
            audio = $('#audio1').bind('play', function () {
                playing = true;
                npAction.text('Mulai');
            }).bind('pause', function () {
                playing = false;
                npAction.text('Stop');
            }).bind('ended', function () {
                npAction.text('Stop');
                if ((index + 1) < trackCount) {
                    index++;
                    loadTrack(index);
                    audio.play();
                } else {
                    audio.pause();
                    index = 0;
                    loadTrack(index);
                }
            }).get(0),
            btnPrev = $('#btnPrev').click(function () {
                if ((index - 1) > -1) {
                    index--;
                    loadTrack(index);
                    if (playing) {
                        audio.play();
                    }
                } else {
                    audio.pause();
                    index = 0;
                    loadTrack(index);
                }
            }),
            btnNext = $('#btnNext').click(function () {
                if ((index + 1) < trackCount) {
                    index++;
                    loadTrack(index);
                    if (playing) {
                        audio.play();
                    }
                } else {
                    audio.pause();
                    index = 0;
                    loadTrack(index);
                }
            }),
            li = $('#plList li').click(function () {
                var id = parseInt($(this).index());
                if (id !== index) {
                    playTrack(id);
                }
            }),
            loadTrack = function (id) {
                $('.plSel').removeClass('plSel');
                $('#plList li:eq(' + id + ')').addClass('plSel');
                npTitle.text(tracks[id].name);
                index = id;
                audio.src = mediaPath + tracks[id].file + extension;
            },
            playTrack = function (id) {
                loadTrack(id);
                audio.play();
            };
        extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
        loadTrack(index);
    }
});



//hhhhhhhhhhhhhhhhhhhh
$(document).ready(function() {
    $('#example').DataTable();
    });


$(document).ready(function() {

	$('select[name=konten_umum]').val('<?php echo $data_row['kode_konten_umum'] ?>');
	$('select[name=sumber_data]').val('<?php echo $data_row['kode_sumber_data'] ?>');
	$('select[name=kategori_musik]').val('<?php echo $data_row['kode_jenis_musik'] ?>');
	
	$('select[name=fungsi_musik]').val('<?php echo $data_row['kode_fungsi_musik'] ?>');
	
	ambil_fungsi_musik();

	

	//$('select[name=vocal_instrumen]').val('');
	//$('select[name=detail_musik]').val('');
	
	//$('.selectpicker').selectpicker('refresh')


	$('.mobileSlider').flexslider({
		animation: "slide",
		slideshowSpeed: 3000,
		controlNav: false,
		directionNav: true,
		prevText: "&#171;",
		nextText: "&#187;"
	});
	$('.flexslider').flexslider({
		animation: "slide",
		directionNav: false
	});
		
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if ($(window).width() < 768) {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar-header').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}
			else {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}

		}
	});
	
	$('#toTop').click(function() {
		$('html,body').animate({
			scrollTop: 0
		}, 1000);
	});
	
	var timer;
    $(window).bind('scroll',function () {
        clearTimeout(timer);
        timer = setTimeout( refresh , 50 );
    });
    var refresh = function () {
		if ($(window).scrollTop()>100) {
			$(".tagline").fadeTo( "slow", 0 );
		}
		else {
			$(".tagline").fadeTo( "slow", 1 );
		}
    };

    
    ambil_form_vocal_instrumen('<?php echo $data_row['kode_vocal_intrument'] ?>','<?php echo $data_row['id_detail_musik'] ?>');
   
});
 function ambil_fungsi_musik()
  {
      $("#detail_musik").empty();
      var value = $("#fungsi_musik").val();
      $.post('ambil_fungsi_musik.php', {
            id: value,
            //kode:kode
      }, function(data, status){
          //alert(data);
          $("#detail_musik").html(data);

      });
  }

  
  function ambil_form_vocal_instrumen(kode, kode_detail)
  {
      $("#tmp_form_vocal").empty();
      var value = $("#kategori_musik").val();
      $.post('ambil_form_vocal_instrumen.php', {
            id: value,
            kode: kode,
            kode_detail: kode_detail
      }, function(data, status){
          //alert(data);
          $("#tmp_form_vocal").html(data);

      });
  }

  function ambil_form_asing()
  {
  	$("#tmp_form_asing").empty();
      var value = $("#sumber_data").val();
      $.post('ambil_form_asing.php', {
            id: value
      }, function(data, status){
          //alert(data);
          $("#tmp_form_asing").html(data);

      });
  }


</script>
  </body>
</html>