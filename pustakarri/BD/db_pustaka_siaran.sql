-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2016 at 10:46 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pustaka_siaran`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_asing`
--

CREATE TABLE `m_asing` (
  `kode` smallint(2) NOT NULL,
  `text` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_asing`
--

INSERT INTO `m_asing` (`kode`, `text`) VALUES
(1, 'Asia'),
(2, 'Amerika'),
(3, 'Australia'),
(4, 'Afrika'),
(5, 'Eropa');

-- --------------------------------------------------------

--
-- Table structure for table `m_detail_fungsi_musik`
--

CREATE TABLE `m_detail_fungsi_musik` (
  `id` smallint(2) NOT NULL,
  `kode_fungsi_musik` varchar(3) DEFAULT NULL,
  `kode` smallint(2) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_detail_fungsi_musik`
--

INSERT INTO `m_detail_fungsi_musik` (`id`, `kode_fungsi_musik`, `kode`, `text`) VALUES
(1, 'a', 1, 'Musik Alternatif'),
(2, 'a', 2, 'Musik Dangdut'),
(3, 'a', 3, 'Musik Jazz'),
(4, 'a', 4, 'Musik Keroncong'),
(5, 'a', 5, 'Musik Klasik'),
(6, 'a', 6, 'Musik Orkestra/Symphoni'),
(7, 'a', 7, 'Musik pop'),
(8, 'a', 8, 'Musik Rock'),
(9, 'a', 9, 'Anak - Anak'),
(10, 'a', 10, 'Musik Tradisional'),
(11, 'b', 1, 'Cepat Pendek'),
(12, 'b', 2, 'Cepat Sedang'),
(13, 'b', 3, 'Cepat Panjang'),
(14, 'b', 4, 'Sedang Pendek'),
(15, 'b', 5, 'Sedang Sedang'),
(16, 'b', 6, 'Sedang Panjang'),
(17, 'b', 7, 'Lambat Pendek'),
(18, 'b', 8, 'Lambat Sedang'),
(19, 'b', 9, 'Lambat Panjang'),
(20, 'c', 1, 'Iringan Tari'),
(21, 'c', 2, 'Upacara'),
(22, 'c', 3, 'Senam'),
(23, 'd', 1, 'Lagu Kebangsaan'),
(24, 'd', 2, 'Lagu Perjuangan'),
(25, 'd', 3, 'Lagu Bernilai Sejarah'),
(26, 'd', 4, 'Religius');

-- --------------------------------------------------------

--
-- Table structure for table `m_fungsi_musik`
--

CREATE TABLE `m_fungsi_musik` (
  `kode` varchar(3) NOT NULL,
  `text` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_fungsi_musik`
--

INSERT INTO `m_fungsi_musik` (`kode`, `text`) VALUES
('a', 'Hiburan'),
('b', 'Ilustrasi'),
('c', 'iringan'),
('d', 'khusus');

-- --------------------------------------------------------

--
-- Table structure for table `m_jenis_instrument`
--

CREATE TABLE `m_jenis_instrument` (
  `kode` smallint(2) NOT NULL,
  `text` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jenis_instrument`
--

INSERT INTO `m_jenis_instrument` (`kode`, `text`) VALUES
(1, 'Mandiri Bernada'),
(2, 'Mandiri Tanpa Nada'),
(3, 'Berperangkat Baku'),
(4, 'Berperangkat Khusus');

-- --------------------------------------------------------

--
-- Table structure for table `m_jenis_musik`
--

CREATE TABLE `m_jenis_musik` (
  `kode` varchar(3) NOT NULL,
  `text` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jenis_musik`
--

INSERT INTO `m_jenis_musik` (`kode`, `text`) VALUES
('A', 'Kategori Musik Vocal'),
('B', 'Kategori Musik Instrument'),
('C', 'Kategori Musik Campuran');

-- --------------------------------------------------------

--
-- Table structure for table `m_jenis_penyimpanan`
--

CREATE TABLE `m_jenis_penyimpanan` (
  `id` smallint(2) NOT NULL,
  `jenis_penyimpanan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jenis_penyimpanan`
--

INSERT INTO `m_jenis_penyimpanan` (`id`, `jenis_penyimpanan`) VALUES
(1, 'CD'),
(2, 'Piringan Hitam'),
(3, 'Kaset');

-- --------------------------------------------------------

--
-- Table structure for table `m_jenis_vocal`
--

CREATE TABLE `m_jenis_vocal` (
  `kode` smallint(2) DEFAULT NULL,
  `text` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jenis_vocal`
--

INSERT INTO `m_jenis_vocal` (`kode`, `text`) VALUES
(1, 'Vocal Solo Wanita'),
(2, 'Vocal Solo Pria'),
(3, 'Vocal Duet Wanita Pria'),
(4, 'Vocal Trio sd Group Wanita'),
(5, 'Vocal Trio sd Group Pria'),
(6, 'Vocal Paduan Suara');

-- --------------------------------------------------------

--
-- Table structure for table `m_konten_umum`
--

CREATE TABLE `m_konten_umum` (
  `kode` varchar(3) NOT NULL,
  `text` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_konten_umum`
--

INSERT INTO `m_konten_umum` (`kode`, `text`) VALUES
('SFK', 'Sound Efect'),
('SK', 'Siaran Kata'),
('SM', 'Siaran Musik');

-- --------------------------------------------------------

--
-- Table structure for table `m_negara`
--

CREATE TABLE `m_negara` (
  `kode` varchar(3) NOT NULL,
  `kode_benua` smallint(2) NOT NULL,
  `text` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_negara`
--

INSERT INTO `m_negara` (`kode`, `kode_benua`, `text`) VALUES
('INA', 1, 'Indonesia');

-- --------------------------------------------------------

--
-- Table structure for table `m_sumber_data`
--

CREATE TABLE `m_sumber_data` (
  `kode` varchar(3) NOT NULL,
  `text` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_sumber_data`
--

INSERT INTO `m_sumber_data` (`kode`, `text`) VALUES
('I', 'Nasional'),
('II', 'Daerah'),
('III', 'Sunda'),
('IV', 'Asing');

-- --------------------------------------------------------

--
-- Table structure for table `t_admin`
--

CREATE TABLE `t_admin` (
  `id` int(5) NOT NULL,
  `nip` int(15) NOT NULL,
  `pass` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_admin`
--

INSERT INTO `t_admin` (`id`, `nip`, `pass`) VALUES
(1, 1234, '1234');

-- --------------------------------------------------------

--
-- Table structure for table `t_album`
--

CREATE TABLE `t_album` (
  `id` int(5) NOT NULL,
  `nama_album` varchar(100) DEFAULT NULL,
  `tahun_album` varchar(4) DEFAULT NULL,
  `deskripsi` text,
  `penyanyi` varchar(50) DEFAULT NULL,
  `file_src` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_album`
--

INSERT INTO `t_album` (`id`, `nama_album`, `tahun_album`, `deskripsi`, `penyanyi`, `file_src`) VALUES
(1, 'test', 'test', 'test', NULL, 'gambar/1481213931s.jpg'),
(3, 'Second Chance', '2014', 'Second Chance merupakan Album studio kedua Noah yang dirilis pada 31 Desember 2014. Album ini menjadi album terakhir Reza bersama Noah yang memutuskan keluar dari Noah per tanggal 1 Januari 2015. Selanjutnya, Noah akan merilis 3 album package yang berisikan materi lama dari Peterpan yaitu : Bintang Di Surga, Hari Yang Cerah... dan Taman Langit. Total seluruh track adalah 41 lagu.\r\n\r\nSingel di album ini ialah Hero, Seperti Kemarin dan Menunggumu. Di album ini terdapat 12 lagu, 3 lagu baru dan 9 lagu lama yang di aransemen ulang. 3 lagu Second Chance, 3 lagu Sebuah Nama Sebuah Cerita, 5 lagu OST. Alexandria, dan 1 lagu Suara Lainnya.\r\n\r\nDi ketiga lagu baru Noah yaitu, Hero, Seperti Kemarin dan Suara Pikiranku, Noah berkerja sama dengan prdouser kenamaan asal Inggris yaitu, Steve Lillywhite yang merupakan produser dari band U2, The Rolling Stone, The Killers dll.\r\n\r\nAlbum ini hanya dijual di jaringan retail Trans Corp. seperti Trans Fashion, Trans F&B (Coffee Bean, Wendyâ€™s, Baskin Robin), TransVision, Carrefour, Kawasan Trans Studio Bandung & Makassar, Metro Department Store, dan Bank Mega.\r\n\r\nDi dalam album ini terdapat sebuah lagu yang istimewa yaitu, Hero. Yang istimewa adalah dari liriknya lagu ini berbahasa Inggris. Selama ini lagu Peterpan maupun Noah menggunakan lirik berbahasa Indonesia.', NULL, 'gambar/1481216814hqdefault.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_lagu`
--

CREATE TABLE `t_lagu` (
  `id` int(5) NOT NULL,
  `judul_lagu` varchar(50) DEFAULT NULL,
  `id_album` int(5) DEFAULT NULL,
  `penyanyi` varchar(50) DEFAULT NULL,
  `file_src` varchar(255) DEFAULT NULL,
  `durasi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_lagu`
--

INSERT INTO `t_lagu` (`id`, `judul_lagu`, `id_album`, `penyanyi`, `file_src`, `durasi`) VALUES
(1, 'test1a', 1, 'test1a', '1481213955-01 Heavy Birthday feat Sopo Jarwo.mp3', '00:00'),
(2, 'test1b', 1, 'test1b', '1481213976-02 Luar Biasa feat Kemal Palevi_2.mp3', '00:10'),
(10, 'Seperti Kemarin', 3, 'Noah', '1481216896-02.Seperti Kemarin.mp3', '03:48'),
(11, 'Suara Pikiranku', 3, 'Noah', '1481216935-03.Suara Pikiranku.mp3', '04:23'),
(12, 'Langit Tak Mendengar', 3, 'Noah', '1481216991-04.Langit Tak Mendengar.mp3', '04:00'),
(13, 'Membebaniku', 3, 'Noah', '1481217045-05.Membebaniku.mp3', '03:54'),
(14, 'Dilema Besar', 3, 'Noah', '1481217094-06.Dilema Besar.mp3', '04:10'),
(15, 'Walau Habis Terang', 3, 'Noah', '1481217137-07.Walau Habis Terang.mp3', '03:39'),
(16, 'Tak Bisakah', 3, 'Noah', '1481217165-08.Tak Bisakah.mp3', '03:25'),
(17, 'Menunggu Pagi', 3, 'Noah', '1481217205-09.Menunggu Pagi.mp3', '04:12'),
(18, 'Tak Ada Yang Abadi', 3, 'Noah', '1481217273-10.Tak Ada Yang Abadi.mp3', '04:17'),
(19, 'Dara', 3, 'Noah', '1481217303-11.Dara.mp3', '03:32'),
(20, 'Menunggumu', 3, 'Noah', '1481217345-12.Menunggumu.mp3', '03:47');

-- --------------------------------------------------------

--
-- Table structure for table `t_perpus_lagu`
--

CREATE TABLE `t_perpus_lagu` (
  `id` int(11) NOT NULL,
  `kode_konten_umum` varchar(3) DEFAULT NULL,
  `kode_sumber_data` varchar(3) DEFAULT NULL,
  `kode_jenis_musik` varchar(3) DEFAULT NULL,
  `id_detail_fungsi_musik` smallint(2) DEFAULT NULL,
  `kode_vocal_intrument` smallint(2) DEFAULT NULL,
  `kode_negara` varchar(3) DEFAULT NULL,
  `no_urut` varchar(4) DEFAULT NULL,
  `id_jenis_penyimpanan` smallint(2) DEFAULT NULL,
  `id_album` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_perpus_lagu`
--

INSERT INTO `t_perpus_lagu` (`id`, `kode_konten_umum`, `kode_sumber_data`, `kode_jenis_musik`, `id_detail_fungsi_musik`, `kode_vocal_intrument`, `kode_negara`, `no_urut`, `id_jenis_penyimpanan`, `id_album`) VALUES
(1, 'SM', 'I', 'B', 12, 2, NULL, 'test', 1, 1),
(3, 'SM', 'I', 'C', 7, 5, NULL, '0003', 1, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_asing`
--
ALTER TABLE `m_asing`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `kode` (`kode`);

--
-- Indexes for table `m_detail_fungsi_musik`
--
ALTER TABLE `m_detail_fungsi_musik`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_fungsi_musik` (`kode_fungsi_musik`);

--
-- Indexes for table `m_fungsi_musik`
--
ALTER TABLE `m_fungsi_musik`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `m_jenis_instrument`
--
ALTER TABLE `m_jenis_instrument`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `m_jenis_musik`
--
ALTER TABLE `m_jenis_musik`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `m_jenis_penyimpanan`
--
ALTER TABLE `m_jenis_penyimpanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_konten_umum`
--
ALTER TABLE `m_konten_umum`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `m_negara`
--
ALTER TABLE `m_negara`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `kode_benua` (`kode_benua`);

--
-- Indexes for table `m_sumber_data`
--
ALTER TABLE `m_sumber_data`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `t_admin`
--
ALTER TABLE `t_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_album`
--
ALTER TABLE `t_album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_lagu`
--
ALTER TABLE `t_lagu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_album` (`id_album`);

--
-- Indexes for table `t_perpus_lagu`
--
ALTER TABLE `t_perpus_lagu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_konten_umum` (`kode_konten_umum`),
  ADD KEY `kode_sumber_data` (`kode_sumber_data`),
  ADD KEY `kode_jenis_musik` (`kode_jenis_musik`),
  ADD KEY `id_detail_fungsi_musik` (`id_detail_fungsi_musik`),
  ADD KEY `id_jenis_penyimpanan` (`id_jenis_penyimpanan`),
  ADD KEY `id_album` (`id_album`),
  ADD KEY `kode_asing` (`kode_negara`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_detail_fungsi_musik`
--
ALTER TABLE `m_detail_fungsi_musik`
  MODIFY `id` smallint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `t_admin`
--
ALTER TABLE `t_admin`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_album`
--
ALTER TABLE `t_album`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_lagu`
--
ALTER TABLE `t_lagu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `t_perpus_lagu`
--
ALTER TABLE `t_perpus_lagu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `m_detail_fungsi_musik`
--
ALTER TABLE `m_detail_fungsi_musik`
  ADD CONSTRAINT `m_detail_fungsi_musik_ibfk_1` FOREIGN KEY (`kode_fungsi_musik`) REFERENCES `m_fungsi_musik` (`kode`) ON UPDATE CASCADE;

--
-- Constraints for table `m_negara`
--
ALTER TABLE `m_negara`
  ADD CONSTRAINT `m_negara_ibfk_1` FOREIGN KEY (`kode_benua`) REFERENCES `m_asing` (`kode`) ON UPDATE CASCADE;

--
-- Constraints for table `t_lagu`
--
ALTER TABLE `t_lagu`
  ADD CONSTRAINT `t_lagu_ibfk_1` FOREIGN KEY (`id_album`) REFERENCES `t_album` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `t_perpus_lagu`
--
ALTER TABLE `t_perpus_lagu`
  ADD CONSTRAINT `t_perpus_lagu_ibfk_1` FOREIGN KEY (`kode_konten_umum`) REFERENCES `m_konten_umum` (`kode`) ON UPDATE CASCADE,
  ADD CONSTRAINT `t_perpus_lagu_ibfk_2` FOREIGN KEY (`kode_sumber_data`) REFERENCES `m_sumber_data` (`kode`) ON UPDATE CASCADE,
  ADD CONSTRAINT `t_perpus_lagu_ibfk_3` FOREIGN KEY (`kode_jenis_musik`) REFERENCES `m_jenis_musik` (`kode`) ON UPDATE CASCADE,
  ADD CONSTRAINT `t_perpus_lagu_ibfk_4` FOREIGN KEY (`id_detail_fungsi_musik`) REFERENCES `m_detail_fungsi_musik` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `t_perpus_lagu_ibfk_6` FOREIGN KEY (`id_jenis_penyimpanan`) REFERENCES `m_jenis_penyimpanan` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `t_perpus_lagu_ibfk_7` FOREIGN KEY (`id_album`) REFERENCES `t_album` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `t_perpus_lagu_ibfk_8` FOREIGN KEY (`kode_negara`) REFERENCES `m_negara` (`kode`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
