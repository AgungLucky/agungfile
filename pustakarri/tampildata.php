<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title></title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">
			<link href="css/data_table.css" rel="stylesheet">
		
			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">
			
			
			
			
			<link href="assets/fa/css/font-awesome.css" rel="stylesheet">
			<link href='assets/font-google/font1.css' rel='stylesheet' type='text/css'>
			<link href='assets/font-google2/font2.css' rel='stylesheet' type='text/css'>

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>

		<body>
		<!-- Header -->
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#topWrap">
							<span class="fa-stack fa-lg">
								
							</span>
							<span>
							<img src="themes/image/we.png" ><font size="+2" font color="#0066FF">  Pustaka Siaran RRI Bandung  </font>
							</span>
						</a>
					</div>
					<div class="collapse navbar-collapse appiNav">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Beranda</a></li>
							<li><a href="#productWrap">Lagu</a></li>
							<li><a href="#pricingWrap">Artis</a></li>
							<li><a href="#testimonialsWrap">Album</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
				<br>
			</div>
			<!-- // Header -->
<br>
<br>
<br>
<br>
<br>
<br />
<br />
	
	<p align="center" >TAMPIL DATA</p>
<?php
/*
	function koneksi_db(){
	$host = "localhost";
	$database = "db_pustaka_siaran";
	$user = "root";
	$password = "";
	$link = mysqli_connect($host,$user,$password);
	mysqli_select_db($link,$database);
	if(!$link){
		echo "Error : ".mysqli_error($link);
		}
	return $link;

concat("a ","b ") as test

}*/
?> 
	<?php
	include('koneksi/koneksi.php');
	$link = koneksi_db(); 
	$sql = '
	SELECT DISTINCT
t_perpus_lagu.kode_konten_umum, t_perpus_lagu.kode_sumber_data, t_perpus_lagu.kode_jenis_musik, m_detail_fungsi_musik.kode_fungsi_musik, m_detail_fungsi_musik.kode, IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
			(SELECT m_jenis_vocal.kode FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
			(SELECT m_jenis_instrument.kode FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)) as kode_vocal_intrument,
			IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
			(SELECT m_jenis_vocal.text FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
			(SELECT m_jenis_instrument.text FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)) as text_vocal_intrument,
			t_perpus_lagu.no_urut,
			t_perpus_lagu.id_jenis_penyimpanan,
			t_album.nama_album, t_perpus_lagu.id,
			t_album.id as id_album,
			
	CONCAT(
	t_perpus_lagu.kode_konten_umum, ".",
	t_perpus_lagu.kode_sumber_data, ".",
	t_perpus_lagu.kode_jenis_musik, ".",
	m_detail_fungsi_musik.kode_fungsi_musik, ".",
	m_detail_fungsi_musik.kode, ".",
	IF(t_perpus_lagu.kode_jenis_musik="A" or t_perpus_lagu.kode_jenis_musik="C", 
	(SELECT m_jenis_vocal.kode FROM m_jenis_vocal WHERE m_jenis_vocal.kode = t_perpus_lagu.kode_vocal_intrument), 
	(SELECT m_jenis_instrument.kode FROM m_jenis_instrument WHERE m_jenis_instrument.kode = t_perpus_lagu.kode_vocal_intrument)), ".",
	t_perpus_lagu.no_urut
) AS kodedetail	
	
		FROM
			t_perpus_lagu
			INNER JOIN m_detail_fungsi_musik ON m_detail_fungsi_musik.id = t_perpus_lagu.id_detail_fungsi_musik
			INNER JOIN t_album ON t_album.id = t_perpus_lagu.id_album
			LEFT JOIN t_lagu ON t_lagu.id_album = t_album.id
 '; 

 	
	$res=mysqli_query($link,$sql); 
	$banyakrecord=mysqli_num_rows($res); 
		if($banyakrecord>0){ ?> 
		<div align="center">Data Kategori ditemukan sebanyak : <b><?php echo $banyakrecord;?> </b>Record</div> 
		
		<table id="example" style="font-size: medium; margin-left: 0px;">
			<thead style="background-color: #0066FF; color: white;">
				<tr>				
					<th align="center">Konten Umum</th>
					<th align="center">Sumber Data</th>
					<th align="center">Jenis kategori</th>
					<th align="center">fungsi Musik</th>
					<th align="center">Jenis Musik</th>
					<th align="center">vocal</th>
					<th align="center">Nomor Urut</th>
					<th align="center">Kode</th>
					<th align="center">Deskripsi</th>

				</tr> 
			</thead>
			<tbody>
		<?php
		$i=0; 
		while( $data=mysqli_fetch_array($res)){ 
		$i++;
		 ?>
		 
		 <tr>
			  <td align="center"><?php echo $data['kode_konten_umum'];?></td> 
			  <td align="center"><?php echo $data['kode_sumber_data'];?></td>
			  <td align="center"><?php echo $data['kode_jenis_musik'];?></td>
			  <td align="center"><?php echo $data['kode_fungsi_musik'];?></td>
			  <td align="center"><?php echo $data['kode'];?></td>
			  <td align="center"><?php echo $data['kode_vocal_intrument'];?></td>
			  <td align="center"><?php echo $data['no_urut'];?></td>
			  <td align="center"><?php echo $data['kodedetail'];?></td>
			  <td >
				 <div class="bd-example">


				 	<a href="dump_desk.php?id_album=<?php echo $data['id_album'];?>"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Deskripsi</button></a>  
					        
		  <?php
		  
		  } ?>
		  </tbody>
		   </table> 
		   <?php
		   } else echo "Tidak ada data pada tabel Kategori."; 
		   ?>

		 
<br />	
<br />
<br />

	
			<?php include "nav/footer.php" ?>
			</br>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->\
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.js"></script>
	 <script src="js/data_table.js"></script>

<script type="text/javascript">

    
     $(document).ready(function() {
    $('#example').DataTable();
    });
  
$(document).ready(function() {

	$('.mobileSlider').flexslider({
		animation: "slide",
		slideshowSpeed: 3000,
		controlNav: false,
		directionNav: true,
		prevText: "&#171;",
		nextText: "&#187;"
	});
	$('.flexslider').flexslider({
		animation: "slide",
		directionNav: false
	});
		
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if ($(window).width() < 768) {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar-header').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}
			else {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}

		}
	});
	
	$('#toTop').click(function() {
		$('html,body').animate({
			scrollTop: 0
		}, 1000);
	});
	
	var timer;
    $(window).bind('scroll',function () {
        clearTimeout(timer);
        timer = setTimeout( refresh , 50 );
    });
    var refresh = function () {
		if ($(window).scrollTop()>100) {
			$(".tagline").fadeTo( "slow", 0 );
		}
		else {
			$(".tagline").fadeTo( "slow", 1 );
		}
    };
		
});
</script>
  </body>
</html>