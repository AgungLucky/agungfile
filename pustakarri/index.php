<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title>Pustaka Siar RRI Bandung</title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">

			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">

			
			<link href="assets/fa/css/font-awesome.css" rel="stylesheet">
			<link href='assets/font-google/font1.css' rel='stylesheet' type='text/css'>
			<link href='assets/font-google2/font2.css' rel='stylesheet' type='text/css'>

			<!-- CSS Header -->
			<!-- jQuery library (served from Google) -->
			<script src="js/jquery.min.js"></script>
			<!-- bxSlider Javascript file -->
			<script src="slideshow/jquery.bxslider.min.js"></script>
			<!-- bxSlider CSS file -->
			<link href="slideshow/jquery.bxslider.css" rel="stylesheet">

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->

			<script>
			$(document).ready(function(){
				  $('.bxSlider').bxSlider({
						  auto: true,
						});
			});
			</script>

		</head>

		<body>
		
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php">
							<span class="fa-stack fa-lg">
								
							</span>
							<span>
							<img src="themes/image/we.png" ><font size="+2" font color="#0088b0"> Pustaka Siaran RRI Bandung </font>
							</span>
						</a>
					</div>
						<ul class="nav navbar-nav">
							<li><a href="index.php">Beranda</a></li>
							<li><a href="tampilalbum.php">Pustaka Siaran RRI</a></li>
							<li><a href="programa.php">Pro RRI</a></li>
							
						</ul>
				</div><!--/.nav-collapse -->
				<br>
			</div>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
			<div class="row">
			  <div class="col-md-2"></div>

			  <div class="col-md-8">
			  		
			  
						<div id="wrapper">
							<div id="sliderwrapper">
								<ul class="bxSlider">
								  <li><img src="slideshow/images/1.jpg" width="1200 px" height="376 px"></li>
								  <li><img src="slideshow/images/2.jpg" width="1200 px" height="376 px"></li>
								  <li><img src="slideshow/images/3.jpg" width="1200 px" height="376 px"></li>
								  <li><img src="slideshow/images/4.jpg" width="1200 px" height="376 px"></li>
								  <li><img src="slideshow/images/5.jpg" width="1200 px" height="376 px"></li>
								  <li><img src="slideshow/images/6.jpg" width="1200 px" height="376 px"></li>
								  <li><img src="slideshow/images/7.jpg" width="1200 px" height="376 px"></li>
								</ul>
							</div>
						</div>

						<p style="text-align:justify;">
							RRI adalah satu-satunya radio yang menyandang nama negara yang siarannya ditujukan untuk kepentingan bangsa dan negara. RRI sebagai Lembaga Penyiaran Publik yang independen, netral dan tidak komersial yang berfungsi memberikan pelayanan siaran informasi, pendidikan, hiburan yang sehat, kontrol sosial, serta menjaga citra positif bangsa di dunia internasional.
						</p>
						<p style="text-align:justify;">
							Besarnya tugas dan fungsi RRI yang diberikan oleh negara melalui UU no 32 tahun 2002 tentang Penyiaran, PP 11 tahun 2005 tentang Lembaga Penyiaran Publik, serta PP 12 tahun 2005,  RRI  dikukuhkan sebagai satu-satunya lembaga penyiaran  yang dapat berjaringan secara nasional  dan dapat bekerja sama dalam siaran dengan lembaga penyiaran Asing.
						</p>
						<p style="text-align:justify">
							Dengan kekuatan  62 stasiun penyiaran termasuk Siaran Luar Negeri dan 5 (lima) satuan kerja (satker) lainnya yaitu Pusat Pemberitaan, Pusat Penelitian dan Pengembangan (Puslitbangdiklat) Satuan Pengawasan Intern, serta diperkuat 16 studio produksi serta 11 perwakilan RRI di Luar negeri  RRI memiliki 61 (enampuluh satu) programa 1, 61 programa 2, 61 programa 3, 14 programa 4 dan 7 studio produksi maka RRI setara dengan 205 stasiun radio.
						</p>
						<p style="text-align:justify">
						<br>
							<b>PRINSIP LEMBAGA PENYIARAN PUBLIK</b>
							<ul>	
							    <li> LPP ADALAH LEMBAGA PENYIARAN UNTUK SEMUA WARGA NEGARA</li>
							    <li> SIARANNYA HARUS MENJANGKAU SELURUH WILAYAH NEGARA</li>
							    <li> SIARANNYA HARUS MEREFLEKSIKAN KEBERAGAMAN</li>
							    <li> SIARANNYA HARUS BERBEDA DG LEMBAGA PENYIARAN LAINNYA</li>
							    <li> LPP HRS MENEGAKKAN INDEPENDENSI DAN NETRALITAS</li>
							    <li> SIARANNYA HARUS BERVARIASI DAN BERKUALITAS TINGGI</li>
							    <li> MENJADI FLAG CARRIER DARI BANGSA INDONESIA</li>
							    <li> MENCERMINKAN IDENTITAS BANGSA</li>
							    <li> PEREKAT DAN PEMERSATU BANGSA</li>
							</ul>	
						</p>
						<p style="text-align:justify">
							<b>VISI LPP RRI</b>
							<br>	
							Menjadikan LPP RRI radio berjaringan terluas, pembangun karakter bangsa, berkelas dunia
						</p>
						<p style="text-align:justify">
							<b>MISI LPP RRI </b>
							<ul>
								<li>
									Memberikan pelayanan informasi terpecaya yang dapat menjadi acuan dan ssarana kontrol sosial masyarakat dengan memperhatikan kode etik jurnalistik/kode etik penyiaran
								</li>
								<li>
									Mengembangkan siaran pendidikan untuk mencerahkan, mencerdaskan, dan memberdayakan serta mendorong kreatifitas masyarakat dalam kerangka membangun karaktek bangsa.
								</li>
								<li>
									Menyelenggarakan siaran yang bertujuan menggali,  melestarikan dan mengembangkan budaya bangsa, memberikan hiburan yang sehat bagi keluarga, membentuk budi pekerti dan jati diri bangsa di tengah arus globalisasi.
								</li>
								<li>
									Menyelenggarakan program siaran berperspektif gender yang sesuai dengan budaya bangsa dan melayani kebutuhan kelompok minoritas.
								</li>
								<li>
									Memperkuat program siaran di wilayah perbatasan untuk menjaga kedaulatan NKRI
								</li>
								<li>
									 Meningkatkan kualitas siaran luar negeri dengan program siaran yang mencerminkan politik negara dan citra positif bangsa.
								</li>
								<li>
									Meningkatkan partisipasi publik dalam proses penyelenggaraan siaran mulai dari tahap perencanaan, pelaksanaan, hingga evaluasi program siaran.
								</li>
								<li>
									Meningkatkan kualitas audio dan memperluas jangkauan siaran secara nasional dan internasional dengan mengoptimalkan sumberdaya teknologi yang ada dan mengadaptasi perkembangan teknologi penyiaran serta mengefisienkan pengelolaan operasional maupun pemeliharaan perangkat teknik.
								</li>
								<li>
									Mengembangkan organisasi yang dinamis, efektif, dan efisien dengan sistem manajemen sumber daya (SDM, keuangan, asset, informasi dan operasional) berbasis teknologi informasi dalam rangka mewujudkan tata kelola lembaga yang baik ( good corporate governance)
								</li>
								<li>
									Meningkatkan kualitas siaran luar negeri dengan program siaran yang mencerminkan politik negara dan citra positif bangsa.
								</li>
								<li>
									Memberikan pelayanan jasa-jasa yang terkait dengan   penggunaan dan pemanfaatan asset negara secara profesional  dan akuntabel serta menggali sumber-sumber penerimaan lain untuk mendukung operasional siaran dan meningkatkan  kesejahteraan pegawai.
								</li>
							</ul>
						</p>

			  </div><!-- class="col-md -->

			  <div class="col-md-2"></div>
			</div>
	
			
		
				
	
	
	<?php include "nav/footer.php" ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
 
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.js"></script>

	
<script type="text/javascript">
$(document).ready(function() {

	$('.mobileSlider').flexslider({
		animation: "slide",
		slideshowSpeed: 3000,
		controlNav: false,
		directionNav: true,
		prevText: "&#171;",
		nextText: "&#187;"
	});
	$('.flexslider').flexslider({
		animation: "slide",
		directionNav: false
	});
		
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if ($(window).width() < 768) {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar-header').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}
			else {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - $('.navbar').outerHeight(true) + 1
					}, 1000);
					return false;
				}
			}

		}
	});
	
	$('#toTop').click(function() {
		$('html,body').animate({
			scrollTop: 0
		}, 1000);
	});
	
	var timer;
    $(window).bind('scroll',function () {
        clearTimeout(timer);
        timer = setTimeout( refresh , 50 );
    });
    var refresh = function () {
		if ($(window).scrollTop()>100) {
			$(".tagline").fadeTo( "slow", 0 );
		}
		else {
			$(".tagline").fadeTo( "slow", 1 );
		}
    };
		
});
</script>
</body>
</html>