<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
			<meta name="description" content="A Bootstrap based app landing page template">
			<meta name="author" content="">
			<link rel="shortcut icon" href="assets/ico/favicon.ico">

			<title> Pustaka Siaran RRI Bandung </title>

			<!-- Bootstrap core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">

			<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">
			<link rel="stylesheet" href="css/style.css" type="text/css">
			
			<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
			<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
			<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>

			<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
		</head>

		<body>
				<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php">
							<span class="fa-stack fa-lg">
								
							</span>
							
							<img src="themes/image/we.png" ><font size="+2" font color="#0088b0"> Pustaka Siaran RRI Bandung </font>
							
						</a>
					</div>
						<ul class="nav navbar-nav">
							<li><a href="index.php">Beranda</a></li>
							<li><a href="sejarah.php">Sejarah RRI</a></li>
							<li><a href="tampilalbum.php">Pustaka Siar RRI</a></li>
							<li><a href="programa.php">Pro RRI</a></li>
							<li><a href="#">Kontak</a></li>
							
						</ul>
				</div><!--/.nav-collapse -->
				<br>
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>
			
	<div class="body">
		<div>
			<div>
				<div>
					<div class="about">
						<div>
							<h2>Sejarah RRI BANDUNG</h2>
							<div>
								<p>
								<div class="frame1">
					<img src="gambar/logo.jpg" alt="">
				</div>					
	<p align="justify">   Radio Republik Indonesia merupakan radio komunitas pertama di nusantara,bermula pada tanggal 2 Mei 1923 bertempat di Bandung. Seorang ahli teknik bernama J.G Prins bersama kawan-kawannya memprakarsai pembuatan Studio Pemancar Radio. Sejak 8 Agustus 1926 siaran perdana dari studio tersebut bisa dinikmati warga kota priangan. Studio radio itu diberi nama De Bandoengsche Radio Vereeniging, yang dibangun oleh Percetakan Corking.
	Sebagai Lembaga Penyiaran Publik, RRI terdiri dari Dewan Direksi. Dewan pengawasan yang berjumlah 5 orang terdiri dari unsur publik. Pemerintah dan RRI. Dewan pengawasan yang merupakan wujud representasi dan supervisi publik memiliki dewan direksi yang berjumlah 5 orang yang bertugas melaksanakan kebijakan penyiaran dan bertanggung jawab atas penyelenggaraan penyiaran. Status sebagai Lembaga Penyiaran Publik juga ditegaskan melalui peraturan pemerintah Nomor 11 dan 12 tahun 2005 yang merupakan penjabaran lebih lanjut dari Undang-undang Nomor 32/2002.
	Fungsi RRI sebagai lembaga penyiaran publik tidak hanya memberikan informasi yang aktual, tepat dan terpercaya. Namun juga memberikan nilai-nilai edukatif seperti memberikan porsi pada siaran pendidikan. Tidak ketinggalan RRI juga menyajikan siaran bernilai seni dan budaya bangsa yang dikemas dalam sajian yang menarik. Hiburan musik manca Negara juga tersaji dalam siaran RRI. Coverage area siaran RRI tidak hanya didalam negeri namun juga menembus sampai manca Negara yang tersaji dalam Voice Of Indonesia (Siaran Luar Negri RRI).</p><br>
	<p align="justify">Saat ini RRI mempunyai kurang lebih 80 stasiun penyiaran dan stasiun penyiaran khusus yang ditunjukan ke Luar Negeri. Kecuali di Jakarta, RRI didaerah hampir seluruhnya menyelenggarakan siaran dalam 3 program yaitu daerah yang melayani segmen masyarakat yang luas sampai pendesaan, Programa kota (Pro II) yang melayani masyarakat diperkotaan dan programa III (Pro III) yang menyajikan Berita dan Informasi (News Channel) kepada masyarakat luas. Di stasiun Cabang Utama Jakarta terdapat 6 Programa II untuk segmen pendengar remaja dan pemuda di Jakarta, Programa III khusus berita dan informasi, Programa IV kebudayaan, Programa V untuk saluran Pendidikan dan Programa VI Musik Klasik dan Bahasa Asing. Sedangkan Suara Indonesia (Voice Of Indonesia) menyelenggarakan siaran dalam 10 bahasa.
	Besarnya tugas dan fungsi RRI yang diberikan oleh negara melalui UU no 32 tahun 2002 tentang Penyiaran, PP 11 tahun 2005 tentang Lembaga Penyiaran Publik, serta PP 12 tahun 2005,  RRI  dikukuhkan sebagai satu-satunya lembaga penyiaran  yang dapat berjaringan secara nasional  dan dapat bekerja sama dalam siaran dengan lembaga penyiaran Asing.
	Dengan kekuatan  62 stasiun penyiaran termasuk Siaran Luar Negeri dan 5 (lima) satuan kerja (satker) lainnya yaitu Pusat Pemberitaan, Pusat Penelitian dan Pengembangan (Puslitbangdiklat) Satuan Pengawasan Intern, serta diperkuat 16 studio produksi serta 11 perwakilan RRI di Luar negeri  RRI memiliki 61 (enampuluh satu) programa 1, 61 programa 2, 61 programa 3, 14 programa 4 dan 7 studio produksi maka RRI setara dengan 205 stasiun.</p>
								
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
			<?php include "nav/footer.php" ?>
		
</body>
</html>